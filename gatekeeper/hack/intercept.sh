#!/usr/bin/env bash

# script to invoke gatekeeper from telepresence intercept

# use local redis container
export USE_REDIS_SENTINEL=false
export REDIS_ADDRESS=localhost:6379
export REDIS_PASSWORD=""

./bin/server