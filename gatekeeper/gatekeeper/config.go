package gatekeeper

import (
	"github.com/gin-gonic/gin"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	configKey = "gatekeeper/serverConfig"
)

type ConfigOptions struct {
	Port                        string
	UseSecureCookie             bool
	CookieName                  string
	CookieSecret                string
	OauthClientID               string
	OauthClientSecret           string
	ServerDomain                string
	GitlabAddr                  string
	GitlabInternalEndpointAddr  string
	GitlabInternalEndpointToken string
	K8sClient                   client.Client
	Namespace                   string
}

// Middleware to set the config options on the context
// for downstream handlers.
func Config(c *ConfigOptions) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(configKey, c)
	}
}

// Helper to get the server config from the context.
func GetConfig(ctx *gin.Context) *ConfigOptions {
	return ctx.MustGet(configKey).(*ConfigOptions)
}
