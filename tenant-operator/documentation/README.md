# Tenant Operator Documentation

* [API docs](./api.md)

## Examples

The following example CRs are provided:

### Tenants

* [Tenant.yaml](../config/examples/Tenant.yaml): Installs all Tenant resources using the default configuration and an Ingress.
