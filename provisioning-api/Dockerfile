FROM --platform=${BUILDPLATFORM} golang:1.20 AS build-prov
ENV CGO_ENABLED=0

ARG GO_BUILD_LDFLAGS

# We can't do incremental caching of go modules, as the individual packages are
# interdependent.
WORKDIR /workspace/scheduler
COPY scheduler/go.mod ./
COPY scheduler/go.sum ./
COPY scheduler/api/ api/
COPY scheduler/controllers/ controllers/
COPY scheduler/version/ version/

WORKDIR /workspace/tenant-operator
COPY tenant-operator/go.mod ./
COPY tenant-operator/go.sum ./
COPY tenant-operator/api/ api/
COPY tenant-operator/controllers/ controllers/
COPY tenant-operator/version/ version/

WORKDIR /workspace/clickhouse-operator
COPY clickhouse-operator/go.mod ./
COPY clickhouse-operator/go.sum ./
COPY clickhouse-operator/api/ api/
COPY clickhouse-operator/controllers/ controllers/

WORKDIR /workspace/go
COPY go/go.mod ./
COPY go/go.sum ./
COPY go/pkg/ pkg/

# cache deps before building and copying source so that we don't need to
# re-download as much and so that source changes don't invalidate our
# downloaded layer
RUN --mount=type=cache,target=/go/pkg go mod download -x

WORKDIR /workspace/provisioning-api
COPY provisioning-api/cmd/ cmd/
COPY provisioning-api/pkg/ pkg/
COPY provisioning-api/go.mod ./
COPY provisioning-api/go.sum ./

ARG TARGETARCH
ARG TARGETOS
# Call go build directly instead of make target because otherwise we have to
# also pull in Makefile.shared from the parent directory and use a wider docker
# build context. Using `bash -c` format to avoid quoting issues.
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags \"$GO_BUILD_LDFLAGS\" -o app ./cmd/"]

FROM gcr.io/distroless/base-debian11:nonroot
COPY --from=build-prov /workspace/provisioning-api/app /

WORKDIR /
USER 65532:65532

ENTRYPOINT ["/app"]
