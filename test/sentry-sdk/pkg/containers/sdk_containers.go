package containers

import (
	"strconv"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

const (
	SentryImageRegistry = "registry.gitlab.com/gitlab-org/opstrace/opstrace/test-with-sentry-sdk"
	sentryImagePath     = SentryImageRegistry + "/sentry-"
)

// GetTestContainers returns a list of testcontainers that we support.
// The tag is appended to the image names to allow various versions.
func GetTestContainers(sentryDSN string, insecure bool, tag string) []testcontainers.ContainerRequest {
	names := []string{
		"go",
		"ruby",
		"python",
		"nodejs",
		"java",
		"rust",
		"php",
		// Add more here when available...
	}

	containers := make([]testcontainers.ContainerRequest, len(names))

	for i, name := range names {
		containers[i] = GetTestContainer(name, sentryDSN, insecure, tag)
	}

	return containers
}

func GetTestContainer(
	lang string,
	sentryDSN string,
	insecure bool,
	tag string,
) testcontainers.ContainerRequest {
	env := map[string]string{
		"SENTRY_DSN":  sentryDSN,
		"INSECURE_OK": strconv.FormatBool(insecure),
	}

	return testcontainers.ContainerRequest{
		Name:       lang,
		Image:      sentryImagePath + lang + ":" + tag,
		Env:        env,
		WaitingFor: wait.ForExit().WithExitTimeout(time.Second * 30),
	}
}
