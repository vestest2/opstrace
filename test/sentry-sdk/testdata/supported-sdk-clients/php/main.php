<?
require_once __DIR__ . '/vendor/autoload.php';

use Http\Discovery\HttpAsyncClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Sentry\Client;
use Sentry\ClientBuilder;
use Sentry\HttpClient\HttpClientFactory;
use Sentry\HttpClient\HttpClientFactoryInterface;
use Sentry\Transport\HttpTransport;
use Sentry\SentrySdk;
use Sentry\Transport\TransportFactoryInterface;
use Sentry\Serializer\PayloadSerializer;
use Http\Client\Curl\Client as CurlHttpClient;

$dsn = $_ENV['SENTRY_DSN'];
$insecure = $_ENV['INSECURE_OK'] == 'true';

$transportFactory = new class($insecure) implements TransportFactoryInterface  {

    private $insecure;

    public function __construct(bool $insecure)
    {
        $this->insecure = $insecure;
    }

    public function create(\Sentry\Options $options): \Sentry\Transport\TransportInterface
    {
        $curlConfig = [
            \CURLOPT_TIMEOUT => $options->getHttpTimeout(),
            \CURLOPT_HTTP_VERSION => $options->isCompressionEnabled() ? \CURL_HTTP_VERSION_1_1 : \CURL_HTTP_VERSION_NONE,
            \CURLOPT_CONNECTTIMEOUT => $options->getHttpConnectTimeout(),
        ];
        
        if ($this->insecure) {
            $curlConfig[\CURLOPT_SSL_VERIFYHOST] = 0;
            $curlConfig[\CURLOPT_SSL_VERIFYPEER] = 0;
        }
        
        if (null !== $options->getHttpProxy()) {
            $curlConfig[\CURLOPT_PROXY] = $options->getHttpProxy();
        }
        
        $httpClient = new CurlHttpClient(null, null, $curlConfig);        

        $httpClientFactory = new HttpClientFactory(
            Psr17FactoryDiscovery::findUriFactory(),
            Psr17FactoryDiscovery::findResponseFactory(),
            Psr17FactoryDiscovery::findStreamFactory(),
            $httpClient,
            'sentry.php',
            Client::SDK_VERSION
        );

        return new HttpTransport($options, 
            $httpClientFactory->create($options),
            Psr17FactoryDiscovery::findStreamFactory(),
            Psr17FactoryDiscovery::findRequestFactory(),
            new PayloadSerializer($options));
    }
};

$builder = ClientBuilder::create([
    'dsn' => $dsn,
    'attach_stacktrace' => true,
]);
$builder->setTransportFactory($transportFactory);

SentrySdk::getCurrentHub()->bindClient($builder->getClient());

Sentry\captureException(new BadMethodCallException('Some Method'));

Sentry\captureMessage('Something went wrong, some message');
?>
