# Sentry-based instrumentation for Go

This sample app demonstrates how to instrument your code using Sentry SDK for Go.

To get started:

* Export your Sentry SDN as an environment variable `SENTRY_DSN`

```
export SENTRY_DSN=http://username@localhost:8080/projects/12345
```

* After that run this program:

```sh
go mod download
...
go run main.go
```

The output from the application should look like:

```
[Sentry] 2023/05/23 11:17:09 Release detection failed: exec: "git": executable file not found in $PATH
[Sentry] 2023/05/23 11:17:09 Some Sentry features will not be available. See https://docs.sentry.io/product/releases/.
[Sentry] 2023/05/23 11:17:09 To stop seeing this message, pass a Release to sentry.Init or set the SENTRY_RELEASE environment variable.
[Sentry] 2023/05/23 11:17:09 Integration installed: ContextifyFrames
[Sentry] 2023/05/23 11:17:09 Integration installed: Environment
[Sentry] 2023/05/23 11:17:09 Integration installed: Modules
[Sentry] 2023/05/23 11:17:09 Integration installed: IgnoreErrors
[Sentry] 2023/05/23 11:17:09 Sending info event [3304498f00b84358af5cb7bade3ce630] to 172.30.0.2 project: 12345
[Sentry] 2023/05/23 11:17:09 Sending error event [ffda9d36108942b299c9dce9bd813b58] to 172.30.0.2 project: 12345
2023/05/23 11:17:09 reported to Sentry: Get "fake-url": unsupported protocol scheme "" - ffda9d36108942b299c9dce9bd813b58
2023/05/23 11:17:09 Sending data to Sentry: 200 OK
2023/05/23 11:17:09 Sending data to Sentry: 200 OK
[Sentry] 2023/05/23 11:17:09 Buffer flushed successfully.
```