// This is an example program that makes an HTTP request and prints response
// headers. Whenever a request fails, the error is reported to Sentry.
//
// Try it by running:
//
//	go run main.go https://sentry.io
//	go run main.go bad-url
//
// To actually report events to Sentry, set the DSN either by editing the
// appropriate line below or setting the environment variable SENTRY_DSN to
// match the DSN of your Sentry project.
package main

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

// release is the release of this program that will be reported to Sentry.
// Use go build -ldflags='-X main.release=VALUE' to set this value in build
// time.
// If not set, a default release value will be derived in runtime from
// environment variables or the Git repository in the current working directory.
var release string

// TransportWithHooks is an http.RoundTripper that wraps an existing
// http.RoundTripper adding hooks that run before and after each round trip.
type TransportWithHooks struct {
	http.RoundTripper
	Before func(*http.Request) error
	After  func(*http.Request, *http.Response, error) (*http.Response, error)
}

func (t *TransportWithHooks) RoundTrip(req *http.Request) (*http.Response, error) {
	if err := t.Before(req); err != nil {
		return nil, err
	}
	resp, err := t.RoundTripper.RoundTrip(req)
	return t.After(req, resp, err)
}

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("usage: %s URL", os.Args[0])
	}

	var tlsConfig = &tls.Config{}
	if os.Getenv("INSECURE_OK") == "true" {
		tlsConfig = &tls.Config{InsecureSkipVerify: true}
	}
	err := sentry.Init(sentry.ClientOptions{
		// Either set your DSN here or set the SENTRY_DSN environment variable.
		// Dsn: "http://glet_SUPERSECRET@localhost:9090/errortracking/api/v1/projects/1",
		// Enable printing of SDK debug messages.
		// Useful when getting started or trying to figure something out.
		Debug:   true,
		Release: release,
		HTTPTransport: &TransportWithHooks{
			RoundTripper: &http.Transport{
				Proxy:                 http.ProxyFromEnvironment,
				IdleConnTimeout:       10 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
				TLSClientConfig:       tlsConfig,
			},
			Before: func(req *http.Request) error {
				return nil
			},
			After: func(req *http.Request, resp *http.Response, err error) (*http.Response, error) {
				if err != nil {
					log.Println("Error while sending data: ", err)
					os.Exit(1)
				}
				log.Printf("Sending data to Sentry: %v\n", resp.Status)
				if resp.StatusCode != http.StatusOK {
					log.Printf("sending error request to Gitlab Observability Backend failed with %v\n", resp.Status)
					os.Exit(1)
				}
				return resp, err
			},
		},
	})
	if err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}
	// Flush buffered events before the program terminates.
	// Set the timeout to the maximum duration the program can afford to wait.
	defer sentry.Flush(2 * time.Second)

	// test sending a message
	sentry.CaptureMessage("Testing CaptureMessage from Go")

	resp, err := http.Get(os.Args[1])
	if err != nil {
		eventID := sentry.CaptureException(err)
		log.Printf("reported to Sentry: %s - %v", err, *eventID)
		return
	}
	defer resp.Body.Close()
}
