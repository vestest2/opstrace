# error-tracking-test Javascript

In order to run this application make sure that `SENTRY_DSN` env var exists

```
export SENTRY_DSN=<SENTRY_DSN>
npm install
node session_crashed.js
node session_exited.js
```

JS Sentry SDK sends 2 session data items to the envelope POST endpoint. One for starting the session with `init = true`
and `status = 'ok'` and depending on the file:
- For session_crashed.js a second session item with `init = false` and `status = 'crashed'`
- For session_exited.js  a second session item with `init = false` and `status = 'exited'`

Example of session_crashed.js
```
// 1st session
{"sent_at":"2023-01-11T12:34:55.080Z","sdk":{"name":"sentry.javascript.node","version":"6.19.7"}}
{"type":"session"}
{
    "sid":"fd198ba6915449ae930ac83686196114",
    "init":true,
    "started":"2023-01-11T12:34:55.076Z",
    "timestamp":"2023-01-11T12:34:55.080Z",
    "status":"ok",
    "errors":1,
    "duration":0.004021406173706055,
    "attrs":{
        "release":"my-javascriprt-project@1.0.0",
        "environment":"dev"
    }
}

// 2nd session
{"sent_at":"2023-01-11T12:34:55.089Z","sdk":{"name":"sentry.javascript.node","version":"6.19.7"}}
{"type":"session"}
{
    "sid":"fd198ba6915449ae930ac83686196114",
    "init":false,
    "started":"2023-01-11T12:34:55.076Z",
    "timestamp":"2023-01-11T12:34:55.089Z",
    "status":"crashed",
    "errors":1,
    "duration":0.01337122917175293,
    "attrs":{
        "release":"my-javascriprt-project@1.0.0",
        "environment":"dev"
    }
}
```

Example of session_exited.js
```
// 1st session
{"sent_at":"2023-01-11T12:14:10.575Z","sdk":{"name":"sentry.javascript.node","version":"6.19.7"}}
{"type":"session"}
{
    "sid":"c44415af2547435f95e7c32c3b9d4d5e",
    "init":true,
    "started":"2023-01-11T12:14:10.572Z",
    "timestamp":"2023-01-11T12:14:10.575Z",
    "status":"ok",
    "errors":1,
    "duration":0.0034067630767822266,
    "attrs":{
        "release":"my-javascriprt-project@1.0.0"
    }
}

// 2nd session
{"sent_at":"2023-01-11T12:14:10.916Z","sdk":{"name":"sentry.javascript.node","version":"6.19.7"}}
{"type":"session"}
{
    "sid":"c44415af2547435f95e7c32c3b9d4d5e",
    "init":false,
    "started":"2023-01-11T12:14:10.572Z",
    "timestamp":"2023-01-11T12:14:10.916Z",
    "status":"exited",
    "errors":1,
    "duration":0.3446066379547119,
    "attrs":{
        "release":"my-javascriprt-project@1.0.0"
    }
}

```