package applogger

import (
	"strings"

	"github.com/sirupsen/logrus"
)

var (
	logger *logrus.Entry
)

func Init() {
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetLevel(logrus.ErrorLevel)
}

func Get() *logrus.Entry {
	if logger == nil {
		logger = logrus.WithFields(logrus.Fields{
			"source": "errortracking-smoketests",
		})
	}
	return logger
}

func SetLevel(lvl string) int {
	switch strings.ToUpper(lvl) {
	case "TRACE":
		logrus.SetLevel(logrus.TraceLevel)
		return int(logrus.TraceLevel)
	case "DEBUG":
		logrus.SetLevel(logrus.DebugLevel)
		return int(logrus.DebugLevel)
	case "INFO":
		logrus.SetLevel(logrus.InfoLevel)
		return int(logrus.InfoLevel)
	case "WARN":
		logrus.SetLevel(logrus.WarnLevel)
		return int(logrus.WarnLevel)
	case "ERROR":
		logrus.SetLevel(logrus.ErrorLevel)
		return int(logrus.ErrorLevel)
	case "FATAL":
		logrus.SetLevel(logrus.FatalLevel)
		return int(logrus.FatalLevel)
	default:
		logrus.SetLevel(logrus.InfoLevel)
		return int(logrus.InfoLevel)
	}
}
