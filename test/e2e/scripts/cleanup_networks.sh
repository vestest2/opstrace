#!/usr/bin/env bash

# Attempt to cleanup various GCP E2E created networking in the case of
# terragrunt failing to do so.
# This does not try to delete GKE clusters or VMs.

# don't -e as we expect some of these commands to fail.
# resources attached to things like VMs should not be deleted but instead just error.
set -u

project_id=${PROJECT_ID:?GCP project ID must be set}
echo "Project ID: $project_id"

region=${REGION:?GCP region must be set}
echo "Region: $region"

# delete all subnets - subnets attached to VMs won't get deleted.
subnets="$(gcloud compute networks subnets list --project "$project_id" --filter="name~'^testplatform-.*'" --format="value(name)")"
if [[ -n "$subnets" ]]; then
  echo "Subnets to delete:"
  echo "$subnets"
  echo "$subnets" | xargs gcloud compute networks subnets delete --quiet --project "$project_id" --region "$region"
else
  echo "No subnets to delete"
fi

# delete all firewalls - firewalls attached to VMs won't get deleted.
firewalls="$(gcloud compute firewall-rules list --project "$project_id" --filter="network~'.*testplatform-.*'" --format="value(name)")"
if [[ -n "$firewalls" ]]; then
  echo "Firewalls to delete:"
  echo "$firewalls"
  echo "$firewalls" | xargs gcloud compute firewall-rules delete --quiet --project "$project_id"
else
  echo "No firewalls to delete"
fi

# delete all addresses
addresses="$(gcloud compute addresses list --project "$project_id" --filter="name~'^testplatform-.*'" --format="value(name)")"
if [[ -n "$addresses" ]]; then
  echo "Addresses to delete:"
  echo "$addresses"
  echo "$addresses" | xargs gcloud compute addresses delete --quiet --project "$project_id" --region "$region"
else
  echo "No addresses to delete"
fi

# global addresses
global_addresses="$(gcloud compute addresses list --project "$project_id" --filter="name~'google-managed-services-testplatform-.*'" --format="value(name)" --global)"
if [[ -n "$global_addresses" ]]; then
  echo "Global addresses to delete:"
  echo "$global_addresses"
  echo "$global_addresses" | xargs gcloud compute addresses delete --quiet --project "$project_id" --global
else
  echo "No global addresses to delete"
fi

# delete all routes
routes="$(gcloud compute routes list --project "$project_id" --filter="network~'.*testplatform-.*'" --format="value(name)")"
if [[ -n "$routes" ]]; then
  echo "Routes to delete:"
  echo "$routes"
  echo "$routes" | xargs gcloud compute routes delete --quiet --project "$project_id"
else
  echo "No routes to delete"
fi

# delete all VPC peerings
peerings_json="$(gcloud compute networks peerings list --project "$project_id" --filter="name~'^testplatform-.*'" --format=json)"
if [[ "$peerings_json" != "[]" ]]; then
  # assumed single nested peering per VPC
  peerings="$(echo "$peerings_json" | jq 'map({vpc: .name, name: .peerings[0].name})')"
  echo "Peerings to delete:"
  echo "$peerings" | jq '.[].name'
  tsv="$(echo "$peerings" | jq -r '.[] | [.name, .vpc] | @tsv')"
  echo "$tsv" | while IFS=$'\t' read -r name vpc; do
    gcloud compute networks peerings delete "$name" --project "$project_id" --network "$vpc"
  done
else
  echo "No peerings to delete"
fi

routers="$(gcloud compute routers list --project "$project_id" --regions "$region" --filter="name~'^testplatform-.*'" --format="value(name)")"
if [[ -n "$routers" ]]; then
  echo "Routers to delete:"
  echo "$routers"
  echo "$routers" | xargs gcloud compute routers delete --quiet --project "$project_id" --region "$region"
else
  echo "No routers to delete"
fi

# try and delete all the VPCs - this won't delete VPCs where there are still subnets/firewalls/etc attached.
vpcs="$(gcloud compute networks list --project "$project_id" --filter="name~'^testplatform-.*'" --format="value(name)")"
if [[ -n "$vpcs" ]]; then
  echo "VPCs to delete:"
  echo "$vpcs"
  echo "$vpcs" | xargs gcloud compute networks delete --quiet --project "$project_id"
else
  echo "No VPCs to delete"
fi

exit 0