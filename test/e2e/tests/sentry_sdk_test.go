package tests

import (
	"encoding/json"
	"io"
	"net/http"
	"time"

	"github.com/docker/docker/api/types/container"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/testcontainers/testcontainers-go"
	gocommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/pkg/containers"
)

const (
	// Our tag for the sentry SDK example images generated from the test/sentry-sdk app.
	// Don't use "latest", it will produce inconsistent results.
	// Set env var TEST_SENTRY_IMAGE_TAG to override.
	TestSentryImageTag = "0.3.0-c23155ca"
)

type errorResp struct {
	Status      string `json:"status,omitempty"`
	ProjectID   int    `json:"project_id,omitempty"`
	Fingerprint int    `json:"fingerprint,omitempty"`
	EventCount  int    `json:"event_count,omitempty"`
	Description string `json:"description,omitempty"`
	Actor       string `json:"actor,omitempty"`
}

var _ = Describe("sentry sdk", Ordered, Serial, func() {
	var containerNetworks []string
	var initialErrorCount int
	var tag string

	BeforeAll(func(ctx SpecContext) {
		if testTarget == gocommon.KIND {
			p, err := testcontainers.ProviderDefault.GetProvider()
			Expect(err).NotTo(HaveOccurred())
			if _, err := p.GetNetwork(ctx, testcontainers.NetworkRequest{
				Name: "kind",
			}); err == nil {
				containerNetworks = []string{"kind"}
			}
		}

		// TEST_SENTRY_IMAGE_TAG can be set to override the default tag.
		// useful when developing the test/sentry-sdk apps.
		tag = gocommon.GetEnv("TEST_SENTRY_IMAGE_TAG", TestSentryImageTag)

	})

	DescribeTable("containers should successfully send error to GOB",
		func(ctx SpecContext, lang string) {
			initialErrorCount = countProjectErrors()

			cr := getContainerRequest(lang, tag, containerNetworks)
			container, err := testcontainers.GenericContainer(
				ctx,
				testcontainers.GenericContainerRequest{
					ContainerRequest: cr,
					Started:          true,
				},
			)
			Expect(err).ToNot(HaveOccurred())

			logsReader, err := container.Logs(ctx)
			Expect(err).ToNot(HaveOccurred())
			allLogs, err := io.ReadAll(logsReader)
			Expect(err).ToNot(HaveOccurred())
			GinkgoWriter.Printf("container logs:\n%s", string(allLogs))

			state, err := container.State(ctx)
			Expect(err).ToNot(HaveOccurred())
			Expect(state.ExitCode).To(Equal(0), "non-zero exit code")

			Expect(container.Terminate(ctx)).To(Succeed())

			Eventually(func(g Gomega) {
				// TODO(joe): actually verify error contents here, not just the number.
				g.Expect(countProjectErrors()).To(Equal(initialErrorCount + 1))
			}, time.Second*10, time.Second).Should(Succeed())
		},
		Entry("Python SDK", "python"),
		Entry("Golang SDK", "go"),
		Entry("Ruby SDK", "ruby"),
		Entry("NodeJS SDK", "nodejs"),
		Entry("Java SDK", "java"),
		Entry("Rust SDK", "rust"),
		Entry("PHP SDK", "php"),
	)
})

func countProjectErrors() int {
	GinkgoHelper()

	By("Listing Errors From GOB")
	GinkgoWriter.Println("List Errors Endpoint", gitLab.errorTrackingConfig.listEndpoint)
	req, err := http.NewRequest(http.MethodGet, gitLab.errorTrackingConfig.listEndpoint, nil)
	Expect(err).ToNot(HaveOccurred())
	req.Header.Add("Private-Token", gitLab.groupReadOnlyToken.Token)
	resp, err := httpClient.Do(req)
	Expect(err).ToNot(HaveOccurred())
	defer resp.Body.Close()
	Expect(resp.StatusCode).To(Equal(http.StatusOK))

	var errors []errorResp
	err = json.NewDecoder(resp.Body).Decode(&errors)
	Expect(err).ToNot(HaveOccurred())

	GinkgoWriter.Printf("list error response: %v\n", errors)

	return len(errors)
}

func getContainerRequest(
	lang string,
	tag string,
	containerNetworks []string,
) testcontainers.ContainerRequest {
	GinkgoHelper()

	cr := containers.GetTestContainer(
		lang,
		gitLab.errorTrackingConfig.clientKey.SentryDsn,
		true,
		tag,
	)
	cr.Networks = containerNetworks

	if testTarget == gocommon.DEVVM {
		cr.HostConfigModifier = func(hostConfig *container.HostConfig) {
			hostConfig.ExtraHosts = []string{
				"gob.devvm:10.15.16.129",
			}
		}
	}
	return cr
}
