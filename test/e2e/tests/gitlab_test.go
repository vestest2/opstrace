package tests

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	gogitlab "github.com/xanzy/go-gitlab"
)

// gitLab specific configuration for the test run
// each run will generate a new project, all other resources
// will only be created if they don't exist.
var gitLab struct {
	// user is set up with the credentials for the test run
	user         *gogitlab.User
	userPassword string
	// user read only token to use for access testing.
	userReadOnlyToken *gogitlab.PersonalAccessToken
	// group is the pre-created group
	group *gogitlab.Group
	// project is the pre-created project in the created group
	project *gogitlab.Project
	// Read-only Observability token for the pre-created group
	groupReadOnlyToken *gogitlab.GroupAccessToken
	// Write-only Observability token for the pre-created group
	groupWriteOnlyToken *gogitlab.GroupAccessToken
	// Read/write Observability token for the pre-created group
	groupReadWriteToken *gogitlab.GroupAccessToken
	// error tracking configuration for the test run
	errorTrackingConfig struct {
		clientKey    *gogitlab.ErrorTrackingClientKey
		listEndpoint string
	}
}

// Init GitLab group/project and sentry related settings.
// Should only be called from suite setup.
func initGitLab(ctx context.Context) {
	Expect(infraConfig.GitLabAdminToken).NotTo(BeEmpty())
	// Test asserts actions using gitlab API like enabling error tracking which shouldn't be done on prod env yet.
	Expect(strings.Index(infraConfig.GitLabHost, "gitlab.com")).To(Equal(-1))

	By("Creating a GitLab API client")
	glClient, err := gogitlab.NewClient(
		infraConfig.GitLabAdminToken,
		gogitlab.WithBaseURL(infraConfig.GitLabAddress()),
		gogitlab.WithHTTPClient(httpClient),
		gogitlab.WithRequestOptions(gogitlab.WithContext(ctx)),
		gogitlab.WithCustomLogger(&glClientLogger{}),
	)
	Expect(err).ToNot(HaveOccurred())

	By("Enabling error tracking on GitLab via application settings")
	req, err := glClient.NewRequest(http.MethodPut, "application/settings", nil, nil)
	Expect(err).ToNot(HaveOccurred())

	q := req.URL.Query()
	q.Add("error_tracking_enabled", "true")
	q.Add("error_tracking_api_url", infraConfig.GOBAddress())
	req.URL.RawQuery = q.Encode()

	_, err = glClient.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Creating a GitLab user")
	// generate a password that GitLab will accept.
	// it doesn't like pure alpha passwords, even if random.
	userPassword := ""
	for i := 0; i < len(testIdentifier); i++ {
		userPassword += fmt.Sprintf("%d%s", i, string(testIdentifier[i]))
	}

	user, _, err := glClient.Users.CreateUser(&gogitlab.CreateUserOptions{
		Email:            gogitlab.String(fmt.Sprintf("test-%s@%s", testIdentifier, infraConfig.GitLabHost)),
		Password:         gogitlab.String(userPassword),
		Username:         gogitlab.String(testIdentifier),
		Name:             gogitlab.String(testIdentifier),
		SkipConfirmation: gogitlab.Bool(true),
	})
	Expect(err).ToNot(HaveOccurred())

	DeferCleanup(func(ctx SpecContext) {
		By("Deleting the test GitLab user")
		_, err := glClient.Users.DeleteUser(user.ID, gogitlab.WithContext(ctx))
		Expect(err).ToNot(HaveOccurred())
	})

	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	scopes := []string{"read_api"}
	pat, _, err := glClient.Users.CreatePersonalAccessToken(user.ID, &gogitlab.CreatePersonalAccessTokenOptions{
		Name:      gogitlab.String("readonly"),
		ExpiresAt: &expiresAt,
		Scopes:    &scopes,
	})
	Expect(err).ToNot(HaveOccurred())

	gitLab.user = user
	gitLab.userPassword = userPassword
	gitLab.userReadOnlyToken = pat

	//nolint:contextcheck // the client already captures the context
	initTestGroup(glClient)
	initErrorTracking(glClient)
}

// Init a test group and project for this test.
// Reuse a single group to keep the number of groups in the system low.
// Create relevant tokens here too.
func initTestGroup(client *gogitlab.Client) {
	var err error

	By("Setting up test group and project")

	By("Get or create test group")
	groupPath := "opstrace-e2e-test-group"
	groupName := "Opstrace E2E Test Group"

	g, resp, err := client.Groups.GetGroup(groupPath, nil)
	if err != nil && resp != nil && resp.StatusCode == http.StatusNotFound {
		gitLab.group, _, err = client.Groups.CreateGroup(&gogitlab.CreateGroupOptions{
			Name: &groupName,
			Path: &groupPath,
		})
		Expect(err).ToNot(HaveOccurred())
	} else {
		Expect(err).ToNot(HaveOccurred())
		gitLab.group = g
	}

	By("Adding the user to the group as a developer")
	_, _, err = client.GroupMembers.AddGroupMember(gitLab.group.ID, &gogitlab.AddGroupMemberOptions{
		UserID:      &gitLab.user.ID,
		AccessLevel: gogitlab.AccessLevel(gogitlab.DeveloperPermissions),
	})
	Expect(err).ToNot(HaveOccurred())

	By("creating a project in the group")
	projectName := fmt.Sprintf("test-%s", testIdentifier)
	gitLab.project, _, err = client.Projects.CreateProject(&gogitlab.CreateProjectOptions{
		Name:        &projectName,
		NamespaceID: &gitLab.group.ID,
	})
	Expect(err).ToNot(HaveOccurred())

	DeferCleanup(func(ctx SpecContext) {
		By("Deleting the test project")
		_, err := client.Projects.DeleteProject(gitLab.project.ID, gogitlab.WithContext(ctx))
		Expect(err).ToNot(HaveOccurred())
	})

	By("Setting up Observability Tokens for the group")
	baseScopes := []string{"read_api"}
	readScope := "read_observability"
	writeScope := "write_observability"
	expiresAt := gogitlab.ISOTime(time.Now().Add(time.Hour * 24))
	readTokenName := fmt.Sprintf("test-e2e-read-%s", testIdentifier)
	writeTokenName := fmt.Sprintf("test-e2e-write-%s", testIdentifier)
	readWriteTokenName := fmt.Sprintf("test-e2e-read-write-%s", testIdentifier)
	createToken := func(name string, scopes []string) *gogitlab.GroupAccessToken {
		t, _, err := client.GroupAccessTokens.CreateGroupAccessToken(
			gitLab.group.ID,
			&gogitlab.CreateGroupAccessTokenOptions{
				Name:      &name,
				Scopes:    &scopes,
				ExpiresAt: &expiresAt,
			})
		Expect(err).ToNot(HaveOccurred())

		DeferCleanup(func(ctx SpecContext) {
			By(fmt.Sprintf("Deleting the test token %s", name))
			_, err := client.GroupAccessTokens.RevokeGroupAccessToken(gitLab.group.ID, t.ID, gogitlab.WithContext(ctx))
			Expect(err).ToNot(HaveOccurred())
		})
		return t
	}
	gitLab.groupReadOnlyToken = createToken(readTokenName, append(baseScopes, readScope))
	gitLab.groupWriteOnlyToken = createToken(writeTokenName, append(baseScopes, writeScope))
	gitLab.groupReadWriteToken = createToken(readWriteTokenName, append(baseScopes, readScope, writeScope))
}

// Init error tracking related settings.
// Requires initGitLab to be called first.
func initErrorTracking(client *gogitlab.Client) {
	var err error

	By("Setting up GitLab Error Tracking")

	By("Enabling error tracking on the project")
	// Enable per-project error tracking and get the Sentry URL from settings and test against it.
	// The go-gitlab client doesn't support this API yet, so we use the raw client.
	req, err := client.NewRequest(
		http.MethodPut,
		fmt.Sprintf("projects/%d/error_tracking/settings", gitLab.project.ID), nil, nil,
	)
	Expect(err).ToNot(HaveOccurred())
	q := req.URL.Query()
	q.Add("active", "true")
	q.Add("integrated", "true")
	req.URL.RawQuery = q.Encode()

	_, err = client.Do(req, nil)
	Expect(err).ToNot(HaveOccurred())

	By("Creating project SENTRY_DSN key")
	key, _, err := client.ErrorTracking.CreateClientKey(gitLab.project.ID, nil)
	Expect(err).ToNot(HaveOccurred())

	listErrorEndpoint := fmt.Sprintf("%s/errortracking/api/v1/projects/%d/errors", infraConfig.GOBAddress(), gitLab.project.ID)

	gitLab.errorTrackingConfig.clientKey = key
	gitLab.errorTrackingConfig.listEndpoint = listErrorEndpoint

	s := spew.Sdump(struct {
		ProjectPath        string
		SentryDSN          string
		ListErrorsEndpoint string
	}{
		ProjectPath:        gitLab.project.PathWithNamespace,
		SentryDSN:          key.SentryDsn,
		ListErrorsEndpoint: listErrorEndpoint,
	})
	By("using error tracking configuration: " + s)
}

type glClientLogger struct{}

func (l *glClientLogger) Printf(format string, v ...interface{}) {
	GinkgoWriter.Printf(format+"\n", v...)
}
