package tests

import (
	"context"
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	. "github.com/onsi/ginkgo/v2"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"go.opentelemetry.io/otel/trace"

	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	query "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/trace-query-api"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

// service name for generated spans compatible query
const tracerServiceName = "test_otlp_tracing_api"

var _ = Describe("Tracing", Ordered, Serial, func() {

	tenant := &schedulerv1alpha1.GitLabObservabilityTenant{}
	beforeAllEnsureGitLabObservabilityTenant(tenant)

	// tracer configured with serviceName to create spans
	var tracer trace.Tracer

	// tokens for accessing the group/project
	var readOnlyToken string
	var writeOnlyToken string

	// send a test span to the otel trace http endpoint
	sendSpans := func(ctx SpecContext, apiKey string, spans ...trace.Span) error {
		opts := []otlptracehttp.Option{
			// #nosec G402
			otlptracehttp.WithTLSClientConfig(&tls.Config{
				InsecureSkipVerify: true,
			}),
			otlptracehttp.WithEndpoint(infraConfig.GOBHost),
			otlptracehttp.WithURLPath(fmt.Sprintf("/v3/%d/%d/ingest/traces", gitLab.group.ID, gitLab.project.ID)),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Private-Token": apiKey,
				},
			),
		}
		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { Expect(exporter.Shutdown(ctx)).To(Succeed()) }()
		return exporter.ExportSpans(ctx, readSpans(spans...))
	}

	// creates a span from the tracer and returns the span context and span.
	// span properties are randomly assigned.
	getSingleSpan := func(ctx SpecContext, tracer trace.Tracer) (context.Context, trace.Span) {
		spanLabel := getRandomSpanLabel()

		kind := randItem(
			trace.SpanKindUnspecified,
			trace.SpanKindInternal,
			trace.SpanKindServer,
			trace.SpanKindClient,
			trace.SpanKindProducer,
			trace.SpanKindConsumer,
		)

		status := randItem(
			codes.Unset,
			codes.Ok,
			codes.Error,
		)

		// provide a unique name for lookup convenience
		operationName := "test_" + spanLabel
		spanCtx, span := tracer.Start(ctx, operationName,
			trace.WithSpanKind(kind),
			trace.WithAttributes(
				attribute.String("testing", spanLabel),
			))
		span.AddEvent("emitting test span", trace.WithAttributes(attribute.String("testing", spanLabel)))
		span.SetStatus(status, "test status")

		By(fmt.Sprintf("created span %s for trace %s",
			span.SpanContext().SpanID(), span.SpanContext().TraceID()))

		return spanCtx, span
	}

	makeTraceRequest := func(ctx SpecContext, apiKey string, root trace.Span) *http.Request {
		By("building query for a trace by ID")

		rs := readSpan(root)
		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/v3/query/%d/traces", infraConfig.GOBAddress(), gitLab.project.ID), nil)
		Expect(err).NotTo(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)
		q := req.URL.Query()
		traceID := rs.SpanContext().TraceID().String()
		q.Add("trace_id", traceID)
		req.URL.RawQuery = q.Encode()

		return req
	}

	queryForTrace := func(ctx SpecContext, apiKey string, root trace.Span) *query.TraceResult {

		rs := readSpan(root)
		req := makeTraceRequest(ctx, apiKey, root)

		var res *query.TraceResult
		// wait for up to ~60s for span to appear in queries.
		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

			traces := &query.TraceResult{}
			g.Expect(json.NewDecoder(resp.Body).Decode(traces)).To(Succeed())
			g.Expect(traces.ProjectID).To(BeNumerically("==", gitLab.project.ID), "trace project id")
			g.Expect(traces.TotalTraces).To(BeNumerically("==", 1), "total traces")
			g.Expect(traces.Traces).To(HaveLen(1), "traces")

			res = traces
		}).WithTimeout(time.Minute).Should(Succeed())

		trace := res.Traces[0]
		hexStringsEqual(trace.TraceID, rs.SpanContext().TraceID().String(), "trace id")
		Expect(trace.Timestamp.UnixNano()).To(Equal(rs.StartTime().UnixNano()), "start time")

		return res
	}

	makeTracer := func(serviceName string) trace.Tracer {
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(serviceName),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).NotTo(HaveOccurred())
		tp := sdktrace.NewTracerProvider(sdktrace.WithResource(r))
		return tp.Tracer("otel_tracer")
	}

	BeforeAll(func(ctx SpecContext) {
		By("set up a shared tracer for our tests")
		tracer = makeTracer(tracerServiceName)

		readOnlyToken = gitLab.groupReadOnlyToken.Token
		writeOnlyToken = gitLab.groupWriteOnlyToken.Token
	})

	Context("Auth handling", func() {
		Context("Ingest", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "", span)).NotTo(Succeed())
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, "bad"+writeOnlyToken, span)).NotTo(Succeed())
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, readOnlyToken, span)).NotTo(Succeed())
			})
		})

		Context("Query", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, "", span))
				Expect(err).NotTo(HaveOccurred())
				By("check we got the GitLab login redirect")
				Expect(res).To(HaveHTTPStatus(http.StatusOK))
				Expect(res.Request.URL.Path).To(Equal("/users/sign_in"))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, "bad"+readOnlyToken, span))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				_, span := getSingleSpan(ctx, tracer)
				span.End()
				Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
				res, err := httpClient.Do(makeTraceRequest(ctx, writeOnlyToken, span))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusForbidden))
			})
		})
	})

	Context("Spans", func() {
		It("can save and retrieve single span", func(ctx SpecContext) {
			_, span := getSingleSpan(ctx, tracer)
			span.End()
			Expect(sendSpans(ctx, writeOnlyToken, span)).To(Succeed())
			t := queryForTrace(ctx, readOnlyToken, span)
			Expect(t.Traces[0].Spans).To(HaveLen(1))
			verifySpan(readSpan(span), t.Traces[0].Spans[0])
		})

		It("can save and retrieve fragmented spans", func(ctx SpecContext) {
			By("create chain of nested spans")
			rootContext, rootSpan := getSingleSpan(ctx, tracer)
			rootSpan.SetAttributes(attribute.String("fragment", "root"))
			rootSpan.AddEvent("emitting root test span")

			childContext, childSpan := tracer.Start(rootContext,
				"test_"+getRandomSpanLabel(),
				trace.WithSpanKind(trace.SpanKindServer))
			childSpan.SetAttributes(attribute.String("fragment", "child"))
			childSpan.AddEvent("emitting child test span")

			_, childSiblingSpan := tracer.Start(rootContext,
				"test"+getRandomSpanLabel(),
				trace.WithLinks(trace.Link{SpanContext: childSpan.SpanContext()}),
				trace.WithSpanKind(trace.SpanKindProducer))

			_, grandchildSpan := tracer.Start(childContext, "test_"+getRandomSpanLabel())
			grandchildSpan.SetAttributes(attribute.String("fragment", "grandchild"))
			grandchildSpan.AddEvent("emitting grandchild test span")
			grandchildSpan.RecordError(errors.New("error"))
			grandchildSpan.SetStatus(codes.Error, "error")

			grandchildSpan.End()
			childSpan.End()
			rootSpan.End()
			childSiblingSpan.End()

			getTraceID := func(span trace.Span) trace.TraceID {
				return readSpan(span).SpanContext().TraceID()
			}

			traceID := getTraceID(rootSpan)
			// ensure all the trace ids are identical
			Expect(
				[]trace.TraceID{getTraceID(rootSpan), getTraceID(childSpan), getTraceID(grandchildSpan)}).
				Should(HaveEach(traceID))

			By("sending spans separately")
			Expect(sendSpans(ctx, writeOnlyToken, rootSpan)).To(Succeed())
			Expect(sendSpans(ctx, writeOnlyToken, childSpan, childSiblingSpan)).To(Succeed())
			Expect(sendSpans(ctx, writeOnlyToken, grandchildSpan)).To(Succeed())

			By("query by trace id")
			var result *query.TraceResult
			// allow 1m for spans to appear
			Eventually(func(g Gomega) {
				res := queryForTrace(ctx, readOnlyToken, rootSpan)
				g.Expect(res.Traces[0].Spans).To(HaveLen(4), "total spans")
				result = res
			}).WithTimeout(time.Minute).Should(Succeed())

			spew.Dump(result)

			By("check returned spans match ours and have correct parent references")
			spans := map[string]query.SpanItem{}
			for _, s := range result.Traces[0].Spans {
				spans[normalizeHexString(s.SpanID)] = s
			}

			// expected span chain to verify parent id references
			spanChain := readSpans(rootSpan, childSpan, grandchildSpan)
			for _, s := range spanChain {
				sID := s.SpanContext().SpanID().String()
				gotSpan, ok := spans[strings.ToUpper(sID)]
				Expect(ok).To(BeTrue(), "span map lookup %s", sID)
				if s.Parent().HasSpanID() {
					hexStringsEqual(gotSpan.ParentSpanID, s.Parent().SpanID().String())
				}
				verifySpan(s, gotSpan)
			}
		})
	})

	Context("Query", func() {
		It("can get unique service names", func(ctx SpecContext) {
			By("create tracers with different service names")
			_, s1 := getSingleSpan(ctx, tracer)
			t2 := makeTracer(tracerServiceName + "_2")
			_, s2 := getSingleSpan(ctx, t2)
			t3 := makeTracer(tracerServiceName + "_3")
			_, s3 := getSingleSpan(ctx, t3)

			s1.End()
			s2.End()
			s3.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2, s3)).To(Succeed())

			By("querying for service names")
			req, err := http.NewRequest("GET", fmt.Sprintf("%s/v3/query/%d/services", infraConfig.GOBAddress(), gitLab.project.ID), nil)
			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				g.Expect(res).To(HaveHTTPBody(MatchJSON(`{
					"services": [
						{
							"name": "test_otlp_tracing_api"
						},
						{
							"name": "test_otlp_tracing_api_2"
						},
						{
							"name": "test_otlp_tracing_api_3"
						}
					]
				}`)))
			}, "10s").Should(Succeed())
		})

		It("can get unique operation names", func(ctx SpecContext) {
			By("creating spans with different operation names")
			service := tracerServiceName + "_opstest"
			t := makeTracer(service)
			c1, s1 := t.Start(ctx, "op1")
			_, s2 := t.Start(c1, "op2")
			s1.End()
			s2.End()

			Expect(sendSpans(ctx, writeOnlyToken, s1, s2)).To(Succeed())
			By("querying for operation names")
			req, err := http.NewRequest("GET",
				fmt.Sprintf("%s/v3/query/%d/services/%s/operations", infraConfig.GOBAddress(), gitLab.project.ID, service), nil)
			Expect(err).NotTo(HaveOccurred())
			req.Header.Set("Private-Token", readOnlyToken)

			Eventually(ctx, func(g Gomega) {
				res, err := httpClient.Do(req)
				g.Expect(err).NotTo(HaveOccurred())
				defer res.Body.Close()

				g.Expect(res).To(HaveHTTPStatus(http.StatusOK))
				g.Expect(res).To(HaveHTTPBody(MatchJSON(`{
					"operations": [
						{
							"name": "op1"
						},
						{
							"name": "op2"
						}
					]
				}`)))
			}, "10s").Should(Succeed())
		})
	})
})

func readSpans(span ...trace.Span) []sdktrace.ReadOnlySpan {
	spans := make([]sdktrace.ReadOnlySpan, len(span))
	for i, s := range span {
		spans[i] = readSpan(s)
	}
	return spans
}

func readSpan(span trace.Span) sdktrace.ReadOnlySpan {
	ro, ok := span.(sdktrace.ReadOnlySpan)
	Expect(ok).To(BeTrue(), "convert to readonly span")
	return ro
}

func getRandomSpanLabel() string {
	return common.RandStringRunes(10)
}

func randItem[A any](items ...A) A {
	//#nosec
	return items[rand.Intn(len(items))]
}

// verify a generated OTEL span against a trace result from our query API
func verifySpan(a sdktrace.ReadOnlySpan, b query.SpanItem) {
	tid := a.SpanContext().TraceID()
	hexStringsEqual(tid.String(), b.TraceID, "trace id")
	Expect(a.Name()).To(Equal(b.Operation), "span name")

	sid := a.SpanContext().SpanID()
	hexStringsEqual(sid.String(), b.SpanID, "span id")

	if a.Parent().IsValid() {
		pid := a.Parent().SpanID()
		hexStringsEqual(pid.String(), b.ParentSpanID, "parent span id")
	}

	Expect(tracerServiceName).To(Equal(b.ServiceName), "service name")

	Expect(a.StartTime().UnixNano()).To(BeEquivalentTo(b.Timestamp.UnixNano()), "start time")
	Expect(a.EndTime().UnixNano()).To(BeEquivalentTo(
		b.Timestamp.Add(time.Duration(b.DurationNano)*time.Nanosecond).UnixNano()), "end time")

	// we return the status code as a string like "STATUS_CODE_OK"
	Expect("STATUS_CODE_"+strings.ToUpper(a.Status().Code.String())).To(Equal(b.StatusCode), "status code")

	// NOTE(joe): various items are not exported from our tracing API
	// These used to be tested when we had a Jaeger backend,
	// referenced here from the OTEL span API:
	// - a.SpanKind()
	// - a.Links()
	// - a.Events()
	// - a.Attributes()
	// - a.SpanContext().TraceState()
	// There are other methods, but these are the most important.
}

// normalize a hex string to remove dashes and uppercase
func normalizeHexString(s string) string {
	return strings.ReplaceAll(s, "-", "")
}

// Check two hex strings are equivalent.
// Trace ids 16 bytes and span ids are 8 bytes.
// We cannot assume everything is a uuid.
// Our trace query service returns TraceIDs formatted as uuids
// and SpanIDs as upper case hex strings.
func hexStringsEqual(a, b string, msg ...interface{}) {
	// remove any - chars from potential uuids
	ah := normalizeHexString(a)
	bh := normalizeHexString(b)
	ab, err := hex.DecodeString(ah)
	Expect(err).NotTo(HaveOccurred())
	bb, err := hex.DecodeString(bh)
	Expect(err).NotTo(HaveOccurred())
	Expect(ab).To(Equal(bb), msg...)
}
