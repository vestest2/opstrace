package gcp

import (
	"fmt"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/testing"
	"github.com/mitchellh/mapstructure"
)

//nolint:lll
var terraformRetryableErrors = map[string]string{
	".*dial tcp .*:443: connect: connection refused": "Temporary error that may occur when the GCP API is not yet available.",
}

type terragruntProvisioner struct {
	config         *configuration
	logger         *logger.Logger
	testIdentifier string
	terragruntDir  string
}

func newTerragruntProvisioner(l *logger.Logger, testIdentifier string, config *configuration) *terragruntProvisioner {
	return &terragruntProvisioner{
		config:         config,
		logger:         l,
		testIdentifier: testIdentifier,
		terragruntDir:  config.terragruntDir("environments/integration"),
	}
}

func (p *terragruntProvisioner) provision(t testing.TestingT) error {
	vs, err := p.config.tfVars.encode()
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:             p.terragruntDir,
		TerraformBinary:          "terragrunt",
		EnvVars:                  vs,
		Logger:                   p.logger,
		RetryableTerraformErrors: terraformRetryableErrors,
	})

	_, err = terraform.RunTerraformCommandE(t, terraformOptions,
		terraform.FormatArgs(terraformOptions, "run-all", "init", "-reconfigure")...)
	if err != nil {
		return fmt.Errorf("terraform init: %w", err)
	}

	_, err = terraform.TgApplyAllE(t, terraformOptions)
	if err != nil {
		return fmt.Errorf("terragrunt apply all: %w", err)
	}

	terraformOptions.TerraformDir += "/gke"
	out, err := terraform.OutputAllE(t, terraformOptions)
	if err != nil {
		return fmt.Errorf("terragrunt outputs: %w", err)
	}
	outputs := &tfOutputs{}
	if err := mapstructure.Decode(out, outputs); err != nil {
		return fmt.Errorf("decode outputs: %w", err)
	}
	p.config.tfOutputs = *outputs

	return nil
}

func (p *terragruntProvisioner) deprovision(t testing.TestingT) error {
	vs, err := p.config.tfVars.encode()
	if err != nil {
		return err
	}

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir:             p.terragruntDir,
		TerraformBinary:          "terragrunt",
		EnvVars:                  vs,
		Logger:                   p.logger,
		RetryableTerraformErrors: terraformRetryableErrors,
	})

	_, err = terraform.RunTerraformCommandE(t, terraformOptions,
		terraform.FormatArgs(terraformOptions, "run-all", "init", "-reconfigure")...)
	if err != nil {
		return fmt.Errorf("terraform init: %w", err)
	}

	_, err = terraform.TgDestroyAllE(t, terraformOptions)
	if err != nil {
		if err := fallbackDestroy(t, terraformOptions); err != nil {
			return fmt.Errorf("terragrunt destroy fallback: %w", err)
		}
		// still error even if the fallback worked, it still indicates broken terraform.
		return fmt.Errorf("terragrunt destroy all: %w", err)
	}
	return nil
}

// Fallback to destroying specific terragrunt modules.
// Ignore opstrace and opstracecluster modules which are doing k8s resource management.
// We'll try and just delete the gcs infra.
func fallbackDestroy(t testing.TestingT, terraformOptions *terraform.Options) error {
	_, err := terraform.RunTerraformCommandE(t, terraformOptions,
		terraform.FormatArgs(terraformOptions, "run-all", "destroy",
			"--terragrunt-exclude-dir", "./opstrace",
			"--terragrunt-exclude-dir", "./cluster")...)
	if err != nil {
		return fmt.Errorf("terragrunt destroy fallback: %w", err)
	}
	return nil
}
