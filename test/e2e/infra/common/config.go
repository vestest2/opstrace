package common

import (
	"fmt"

	"github.com/gruntwork-io/terratest/modules/logger"
	"github.com/gruntwork-io/terratest/modules/testing"
	. "github.com/onsi/ginkgo/v2"
)

// Some shared environment variable names for infra providers.
const (
	TestEnvSchedulerImage = "TEST_SCHEDULER_IMAGE"
	TestEnvGitLabImage    = "TEST_GITLAB_IMAGE"
)

// Configuration is shared config between the different infra providers.
type Configuration struct {
	// GOBHost is the host of the GOB instance
	GOBHost string
	// GOBScheme is the protocol scheme of the GOB instance
	GOBScheme string
	// GitLabHost is the host of the GitLab instance
	GitLabHost string
	// GitLabScheme is the protocol scheme of the GitLab instance
	GitLabScheme string
	// InstanceName is the name of the instance
	InstanceName string
	// GitLabImage is the image used for GitLab
	GitLabImage string
	// SchedulerImage is the image used for the scheduler
	SchedulerImage string
	// GitLabAdminToken is the admin token for the GitLab instance.
	GitLabAdminToken string
}

func (c Configuration) GOBAddress() string {
	p := c.GOBScheme
	if p == "" {
		p = "https"
	}
	return fmt.Sprintf("%s://%s", p, c.GOBHost)
}

func (c Configuration) GitLabAddress() string {
	p := c.GitLabScheme
	if p == "" {
		p = "https"
	}
	return fmt.Sprintf("%s://%s", p, c.GitLabHost)
}

type ginkgoLogger struct{}

func (ginkgoLogger) Logf(t testing.TestingT, format string, args ...interface{}) {
	logger.DoLog(t, 3, GinkgoWriter, fmt.Sprintf(format, args...))
}

var GinkgoLogger = logger.New(ginkgoLogger{})
