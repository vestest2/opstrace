#!/usr/bin/env bash

set -euo pipefail

CURRENT_ASDF_DIR="${ASDF_DIR:-${HOME}/.asdf}"
CURRENT_ASDF_DATA_DIR="${ASDF_DATA_DIR:-${HOME}/.asdf}"

ASDF_VERSION_TO_INSTALL="v0.9.0"

asdf_install() {
    if [[ ! -d "${CURRENT_ASDF_DIR}" ]]; then
        git clone https://github.com/asdf-vm/asdf.git "${CURRENT_ASDF_DIR}" --branch ${ASDF_VERSION_TO_INSTALL}

        asdf_add_initializer "${HOME}/.bashrc" "asdf.sh"
        asdf_add_initializer "${HOME}/.zshrc" "asdf.sh"

        return 0
    else
        echo
        echo "Seems like asdf is already installed in \"${CURRENT_ASDF_DIR}\""
        echo
    fi

    return 0
}

asdf_add_initializer() {
    if [[ -f "${1}" ]]; then
        echo -e "\n# Added by opstrace bootstrap\n. ${CURRENT_ASDF_DIR}/${2}" >> "${1}"

        echo
        echo "INFO: asdf installed successfully!"
        echo
        echo "INFO: To make sure asdf commands are available in this shell, run:"
        echo
        echo "\"source ${1}\""
        echo
        echo "INFO: To then install the rest of Opstrace dependencies, run \"make bootstrap\""
    fi
}

error() {
    echo
    echo "ERROR: ${1}" >&2
    echo
}

####################################################
echo "INFO: Installing asdf..."
if ! asdf_install; then
    error "Failed to install asdf." >&2
fi

