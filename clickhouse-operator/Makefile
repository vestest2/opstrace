include ../Makefile.operator.mk

DOCKER_IMAGE_NAME := clickhouse-operator

.PHONY: e2e-test
e2e-test:
	$(GINKGO) -v --ldflags "$(GO_BUILD_LDFLAGS)" ./e2e

.PHONY: unit-tests
unit-tests: test

.PHONY: kind-create
kind-create:
	kind create cluster --image=$(KIND_IMAGE) --name $(DOCKER_IMAGE_NAME) --config=kind.yaml

.PHONY: kind
# Create cluster, install CRDS and deploy dev deps, for convenience.
# Don't deploy our manager is we may want to run it locally.
kind: kind-create install deploy-dev-dependencies

.PHONY: kind-delete
kind-delete:
	kind delete cluster --name $(DOCKER_IMAGE_NAME)

##@ Build

manifests: manifests-default

.PHONY: build
build: manifests generate fmt vet ## Render CRD yaml and build manager binary.
	go build -ldflags "$(GO_BUILD_LDFLAGS)" -o bin/manager main.go

.PHONY: run
run: manifests generate fmt vet ## Run a controller from your host.
	go run ./main.go

.PHONY: docker-ensure
docker-ensure: docker-ensure-default

.PHONY: docker-build
docker-build: ## Build docker image with the manager.
	docker build -f Dockerfile --build-arg GO_BUILD_LDFLAGS -t ${DOCKER_IMAGE} ..

.PHONY: docker-push
docker-push: ## Push docker image with the manager.
	docker push ${DOCKER_IMAGE}

.PHONY: docker
docker: docker-build docker-push

.PHONY: print-docker-images
print-docker-images: print-docker-image-name-tag

##@ Deployment

ifndef ignore-not-found
  ignore-not-found = false
endif

.PHONY: install
install: manifests ## Install CRDs into the K8s cluster specified in ~/.kube/config. Also deploys certmanager as a prerequisite.
	kubectl apply -k config/crd

.PHONY: uninstall
uninstall: manifests ## Uninstall CRDs from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
	kubectl delete --ignore-not-found=$(ignore-not-found) -k config/crd

.PHONY: deploy
deploy: manifests ## Deploy controller to the K8s cluster specified in ~/.kube/config.
	kubectl apply -k config/default

.PHONY: undeploy
undeploy: ## Undeploy controller from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
	kubectl delete --ignore-not-found=$(ignore-not-found) -k config/default

.PHONY: deploy-certmanager
deploy-certmanager: ## Deploy certmanager for local dev purposes.
	kubectl apply -f ../scheduler/controllers/cluster/manifests/cert-manager/bundle.yaml
	kubectl apply -f ../scheduler/controllers/cluster/manifests/cert-manager/certmanager.crd.yaml

.PHONY: undeploy-certmanager
undeploy-certmanager:
	kubectl delete -f ../scheduler/controllers/cluster/manifests/cert-manager/bundle.yaml
	kubectl delete -f ../scheduler/controllers/cluster/manifests/cert-manager/certmanager.crd.yaml

.PHONY: deploy-minio
deploy-minio: ## Deploy minio for local dev purposes.
	kubectl apply -f config/minio-local

.PHONY: undeploy-minio
undeploy-minio:
	kubectl delete -f config/minio-local

# deploy dev dependencies for working locally
.PHONY: deploy-dev-dependencies
deploy-dev-dependencies: deploy-certmanager deploy-minio

.PHONY: undeploy-dev-dependencies
undeploy-dev-dependencies: undeploy-certmanager undeploy-minio

MC_BIN ?= $(LOCALBIN)/mc
OS ?= $(shell go env GOOS)
ARCH ?= $(shell go env GOARCH)

.PHONY: install-minio-client
install-minio-client: $(MC_BIN) # Installs mc client in bin, only used for local development purposes.
$(MC_BIN): | $(LOCALBIN)
	curl https://dl.min.io/client/mc/release/$(OS)-$(ARCH)/mc --create-dirs -o $(MC_BIN)
	chmod +x $(MC_BIN)
