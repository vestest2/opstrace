# Administrator Guide to ClickHouse Keeper operations

> We will soon be migrating to ClickHouse Cloud so this guide will be deprecated. Details in [this epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/82).

We use [ClickHouse Keeper](https://clickhouse.com/docs/en/guides/sre/keeper/clickhouse-keeper) as a coordination system
for replication and distributed DDL queries.

We recommend to go over the official [docs](https://clickhouse.com/docs/en/guides/sre/keeper/clickhouse-keeper) to get familiarity with Keeper.

In this guide, we cover some of the operations that one may require during administration.
Some steps can be modified to achieve any action that require manual operations with ClickHouse Keeper.

## Case of stuck replication queue entries in ClickHouse

We've seen instances where during a period of a replica being offline along with a storage policy change, there were
entries in replication queue that were stuck and cannot move forward.
Similar issues can be seen here: [1](https://github.com/ClickHouse/ClickHouse/issues/10368), [2](https://github.com/ClickHouse/ClickHouse/issues/14296) and [3](https://github.com/ClickHouse/ClickHouse/issues/34700).
In such cases the entries were failed due to errors like: `NO_REPLICA_HAS_PART`. Most of these instances involve `GET_PART` operation but some reported `MERGE_PART` as well.

In order to look into the entries, one can execute:

```text
SELECT
    database,
    table,
    replica_name,
    node_name,
    num_tries,
    create_time,
    new_part_name,
    last_exception,
    last_exception_time,
    last_attempt_time
FROM system.replication_queue
FORMAT Vertical

Query id: ea7c1646-a79e-4c5a-ab5d-10f9e9976cf1

Row 1:
──────
database:            errortracking_api
table:               gl_error_tracking_error_status_local
replica_name:        cluster-0-0
node_name:           queue-0000377699
num_tries:           8459
create_time:         2023-05-12 14:28:38
new_part_name:       all_0_313840_63766
last_exception:      Code: 234. DB::Exception: No active replica has part all_0_313840_63766 or covering part (cannot execute queue-0000377699: GET_PART with virtual parts [all_0_313840_63766]). (NO_REPLICA_HAS_PART), Stack trace (when copying this message, always include the lines below):

0. DB::Exception::Exception(DB::Exception::MessageMasked&&, int, bool) @ 0xe1e20b5 in /usr/bin/clickhouse
1. ? @ 0x12449420 in /usr/bin/clickhouse
2. DB::StorageReplicatedMergeTree::executeFetch(DB::ReplicatedMergeTreeLogEntry&, bool) @ 0x1408858e in /usr/bin/clickhouse
3. DB::StorageReplicatedMergeTree::executeLogEntry(DB::ReplicatedMergeTreeLogEntry&) @ 0x140757c5 in /usr/bin/clickhouse
4. ? @ 0x1415dc5f in /usr/bin/clickhouse
5. DB::ReplicatedMergeTreeQueue::processEntry(std::function<std::shared_ptr<zkutil::ZooKeeper> ()>, std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&, std::function<bool (std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&)>) @ 0x147dcfa6 in /usr/bin/clickhouse
6. DB::StorageReplicatedMergeTree::processQueueEntry(std::shared_ptr<DB::ReplicatedMergeTreeQueue::SelectedEntry>) @ 0x140b887c in /usr/bin/clickhouse
7. ? @ 0x1415e931 in /usr/bin/clickhouse
8. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::routine(std::shared_ptr<DB::TaskRuntimeData>) @ 0x89ce4cc in /usr/bin/clickhouse
9. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::threadFunction() @ 0x89ce180 in /usr/bin/clickhouse
10. ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::worker(std::__list_iterator<ThreadFromGlobalPoolImpl<false>, void*>) @ 0xe2b5625 in /usr/bin/clickhouse
11. void std::__function::__policy_invoker<void ()>::__call_impl<std::__function::__default_alloc_func<ThreadFromGlobalPoolImpl<false>::ThreadFromGlobalPoolImpl<void ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::scheduleImpl<void>(std::function<void ()>, long, std::optional<unsigned long>, bool)::'lambda0'()>(void&&)::'lambda'(), void ()>>(std::__function::__policy_storage const*) @ 0xe2b8195 in /usr/bin/clickhouse
12. ThreadPoolImpl<std::thread>::worker(std::__list_iterator<std::thread, void*>) @ 0xe2b13f3 in /usr/bin/clickhouse
13. ? @ 0xe2b7061 in /usr/bin/clickhouse
14. ? @ 0x7f9f31de9609 in ?
15. clone @ 0x7f9f31d0e133 in ?
 (version 23.3.2.37 (official build))
last_exception_time: 2023-05-25 16:07:27
last_attempt_time:   2023-05-25 16:07:27

Row 4:
──────
database:            errortracking_api
table:               gl_error_tracking_error_status_v2_local
replica_name:        cluster-0-0
node_name:           queue-0000275521
num_tries:           25311
create_time:         2023-05-12 14:31:55
new_part_name:       all_136683_157229_18156
last_exception:      Code: 234. DB::Exception: No active replica has part all_136683_157229_18156 or covering part (cannot execute queue-0000275521: GET_PART with virtual parts [all_136683_157229_18156]). (NO_REPLICA_HAS_PART), Stack trace (when copying this message, always include the lines below):

0. DB::Exception::Exception(DB::Exception::MessageMasked&&, int, bool) @ 0xe1e20b5 in /usr/bin/clickhouse
1. ? @ 0x12449420 in /usr/bin/clickhouse
2. DB::StorageReplicatedMergeTree::executeFetch(DB::ReplicatedMergeTreeLogEntry&, bool) @ 0x1408858e in /usr/bin/clickhouse
3. DB::StorageReplicatedMergeTree::executeLogEntry(DB::ReplicatedMergeTreeLogEntry&) @ 0x140757c5 in /usr/bin/clickhouse
4. ? @ 0x1415dc5f in /usr/bin/clickhouse
5. DB::ReplicatedMergeTreeQueue::processEntry(std::function<std::shared_ptr<zkutil::ZooKeeper> ()>, std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&, std::function<bool (std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&)>) @ 0x147dcfa6 in /usr/bin/clickhouse
6. DB::StorageReplicatedMergeTree::processQueueEntry(std::shared_ptr<DB::ReplicatedMergeTreeQueue::SelectedEntry>) @ 0x140b887c in /usr/bin/clickhouse
7. ? @ 0x1415e931 in /usr/bin/clickhouse
8. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::routine(std::shared_ptr<DB::TaskRuntimeData>) @ 0x89ce4cc in /usr/bin/clickhouse
9. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::threadFunction() @ 0x89ce180 in /usr/bin/clickhouse
10. ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::worker(std::__list_iterator<ThreadFromGlobalPoolImpl<false>, void*>) @ 0xe2b5625 in /usr/bin/clickhouse
11. void std::__function::__policy_invoker<void ()>::__call_impl<std::__function::__default_alloc_func<ThreadFromGlobalPoolImpl<false>::ThreadFromGlobalPoolImpl<void ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::scheduleImpl<void>(std::function<void ()>, long, std::optional<unsigned long>, bool)::'lambda0'()>(void&&)::'lambda'(), void ()>>(std::__function::__policy_storage const*) @ 0xe2b8195 in /usr/bin/clickhouse
12. ThreadPoolImpl<std::thread>::worker(std::__list_iterator<std::thread, void*>) @ 0xe2b13f3 in /usr/bin/clickhouse
13. ? @ 0xe2b7061 in /usr/bin/clickhouse
14. ? @ 0x7f9f31de9609 in ?
15. clone @ 0x7f9f31d0e133 in ?
 (version 23.3.2.37 (official build))
last_exception_time: 2023-05-25 16:07:29
last_attempt_time:   2023-05-25 16:07:29

Row 5:
──────
database:            errortracking_api
table:               gl_error_tracking_errors_local
replica_name:        cluster-0-0
node_name:           queue-0000196546
num_tries:           24875
create_time:         2023-05-12 14:31:55
new_part_name:       all_0_157230_39273
last_exception:      Code: 234. DB::Exception: No active replica has part all_0_157230_39273 or covering part (cannot execute queue-0000196546: GET_PART with virtual parts [all_0_157230_39273]). (NO_REPLICA_HAS_PART), Stack trace (when copying this message, always include the lines below):

0. DB::Exception::Exception(DB::Exception::MessageMasked&&, int, bool) @ 0xe1e20b5 in /usr/bin/clickhouse
1. ? @ 0x12449420 in /usr/bin/clickhouse
2. DB::StorageReplicatedMergeTree::executeFetch(DB::ReplicatedMergeTreeLogEntry&, bool) @ 0x1408858e in /usr/bin/clickhouse
3. DB::StorageReplicatedMergeTree::executeLogEntry(DB::ReplicatedMergeTreeLogEntry&) @ 0x140757c5 in /usr/bin/clickhouse
4. ? @ 0x1415dc5f in /usr/bin/clickhouse
5. DB::ReplicatedMergeTreeQueue::processEntry(std::function<std::shared_ptr<zkutil::ZooKeeper> ()>, std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&, std::function<bool (std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&)>) @ 0x147dcfa6 in /usr/bin/clickhouse
6. DB::StorageReplicatedMergeTree::processQueueEntry(std::shared_ptr<DB::ReplicatedMergeTreeQueue::SelectedEntry>) @ 0x140b887c in /usr/bin/clickhouse
7. ? @ 0x1415e931 in /usr/bin/clickhouse
8. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::routine(std::shared_ptr<DB::TaskRuntimeData>) @ 0x89ce4cc in /usr/bin/clickhouse
9. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::threadFunction() @ 0x89ce180 in /usr/bin/clickhouse
10. ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::worker(std::__list_iterator<ThreadFromGlobalPoolImpl<false>, void*>) @ 0xe2b5625 in /usr/bin/clickhouse
11. void std::__function::__policy_invoker<void ()>::__call_impl<std::__function::__default_alloc_func<ThreadFromGlobalPoolImpl<false>::ThreadFromGlobalPoolImpl<void ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::scheduleImpl<void>(std::function<void ()>, long, std::optional<unsigned long>, bool)::'lambda0'()>(void&&)::'lambda'(), void ()>>(std::__function::__policy_storage const*) @ 0xe2b8195 in /usr/bin/clickhouse
12. ThreadPoolImpl<std::thread>::worker(std::__list_iterator<std::thread, void*>) @ 0xe2b13f3 in /usr/bin/clickhouse
13. ? @ 0xe2b7061 in /usr/bin/clickhouse
14. ? @ 0x7f9f31de9609 in ?
15. clone @ 0x7f9f31d0e133 in ?
 (version 23.3.2.37 (official build))
last_exception_time: 2023-05-25 16:07:29
last_attempt_time:   2023-05-25 16:07:29

Row 6:
──────
database:            errortracking_api
table:               gl_error_tracking_group_local
replica_name:        cluster-0-0
node_name:           queue-0000000000
num_tries:           9452
create_time:         2023-05-25 14:06:40
new_part_name:       all_0_157230_39275
last_exception:      Code: 234. DB::Exception: No active replica has part all_0_157230_39275 or covering part (cannot execute queue-0000000000: GET_PART with virtual parts [all_0_157230_39275]). (NO_REPLICA_HAS_PART), Stack trace (when copying this message, always include the lines below):

0. DB::Exception::Exception(DB::Exception::MessageMasked&&, int, bool) @ 0xe1e20b5 in /usr/bin/clickhouse
1. ? @ 0x12449420 in /usr/bin/clickhouse
2. DB::StorageReplicatedMergeTree::executeFetch(DB::ReplicatedMergeTreeLogEntry&, bool) @ 0x1408858e in /usr/bin/clickhouse
3. DB::StorageReplicatedMergeTree::executeLogEntry(DB::ReplicatedMergeTreeLogEntry&) @ 0x140757c5 in /usr/bin/clickhouse
4. ? @ 0x1415dc5f in /usr/bin/clickhouse
5. DB::ReplicatedMergeTreeQueue::processEntry(std::function<std::shared_ptr<zkutil::ZooKeeper> ()>, std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&, std::function<bool (std::shared_ptr<DB::ReplicatedMergeTreeLogEntry>&)>) @ 0x147dcfa6 in /usr/bin/clickhouse
6. DB::StorageReplicatedMergeTree::processQueueEntry(std::shared_ptr<DB::ReplicatedMergeTreeQueue::SelectedEntry>) @ 0x140b887c in /usr/bin/clickhouse
7. ? @ 0x1415e931 in /usr/bin/clickhouse
8. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::routine(std::shared_ptr<DB::TaskRuntimeData>) @ 0x89ce4cc in /usr/bin/clickhouse
9. DB::MergeTreeBackgroundExecutor<DB::RoundRobinRuntimeQueue>::threadFunction() @ 0x89ce180 in /usr/bin/clickhouse
10. ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::worker(std::__list_iterator<ThreadFromGlobalPoolImpl<false>, void*>) @ 0xe2b5625 in /usr/bin/clickhouse
11. void std::__function::__policy_invoker<void ()>::__call_impl<std::__function::__default_alloc_func<ThreadFromGlobalPoolImpl<false>::ThreadFromGlobalPoolImpl<void ThreadPoolImpl<ThreadFromGlobalPoolImpl<false>>::scheduleImpl<void>(std::function<void ()>, long, std::optional<unsigned long>, bool)::'lambda0'()>(void&&)::'lambda'(), void ()>>(std::__function::__policy_storage const*) @ 0xe2b8195 in /usr/bin/clickhouse
12. ThreadPoolImpl<std::thread>::worker(std::__list_iterator<std::thread, void*>) @ 0xe2b13f3 in /usr/bin/clickhouse
13. ? @ 0xe2b7061 in /usr/bin/clickhouse
14. ? @ 0x7f9f31de9609 in ?
15. clone @ 0x7f9f31d0e133 in ?
 (version 23.3.2.37 (official build))
last_exception_time: 2023-05-25 16:07:29
last_attempt_time:   2023-05-25 16:07:29
```

We can observe that value of `num_tries` column keeps increasing and no progress is actually being made.

Once we confirm that the operations are stuck, and we need to remove them we can manually delete such entries from ClickHouse Keeper.
Note that this should only be used as a last resort as this can also create few more issues like read only tables which will then need to be fixed by recreating the table.

First we need to form the path for nodes that needs to be deleted.

```sql
SELECT concat(replica_path, '/queue/', node_name)
FROM system.replication_queue
INNER JOIN system.replicas USING (database, table)
WHERE (create_time < (now() - toIntervalDay(1))) AND (type = 'GET_PART') AND (last_exception LIKE '%No active replica has part%')

Query id: eeb96872-3763-4516-8492-b3378172f3dd

┌─concat(replica_path, '/queue/', node_name)─────────────────────────────────────────────────────────────────────────────────────────┐
│ /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_error_status/replicas/cluster-0-0/queue/queue-0000377708          │
│ /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_error_status_v2_local/replicas/cluster-0-0/queue/queue-0000275521 │
│ /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_errors/replicas/cluster-0-0/queue/queue-0000196546                │
│ /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_group_local/replicas/cluster-0-0/queue/queue-0000000000           │
└────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
```

ClickHouse Keeper implements Zookeeper compatible client-server protocol so Zookeeper client can be used to delete such entries.

We can attach a client to running server as:

```bash
kubectl debug --share-processes=true   -it  cluster-0-0-0 --image=zookeeper:latest -- /bin/bash
```

Then execute command line utility as:

```text
I have no name!@cluster-0-0-0:/apache-zookeeper-3.8.1-bin$ zkCli.sh
Connecting to localhost:2181
...
[zk: localhost:2181(CONNECTED) 0]
```

Zookeeper CLI client docs are [here](https://zookeeper.apache.org/doc/r3.8.1/zookeeperCLI.html) which should be referred in case you need a refresher.

### Note: This is a destructive action and should be performed very carefully

Deleting the entries can be done as:

```text
[zk: localhost:2181(CONNECTED) 5] delete /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_error_status_v2_local/replicas/cluster-0-1/queue/queue-0000275492
[zk: localhost:2181(CONNECTED) 9] ls /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_error_status_v2_local/replicas/cluster-0-1/queue/queue-0000275492
Node does not exist: /clickhouse/cluster/tables/0/errortracking_api.gl_error_tracking_error_status_v2_local/replicas/cluster-0-1/queue/queue-0000275492
```

After deleting such entries, we can confirm that stuck entries are removed:

```sql
SELECT concat(replica_path, '/queue/', node_name)
FROM system.replication_queue
INNER JOIN system.replicas USING (database, table)
WHERE (create_time < (now() - toIntervalDay(1))) AND (type = 'GET_PART') AND (last_exception LIKE '%No active replica has part%')

Query id: 878e6f41-dd2b-4fc0-ae2b-0173f7d4dbb6

Ok.

0 rows in set. Elapsed: 0.003 sec.
```
