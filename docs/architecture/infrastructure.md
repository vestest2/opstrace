# Infrastructure overview

This document outlines the environments used for develop/test/deploy on GitLab Observability Backend (GOB).

## Infrastructure as Code

All infrastructure environments are defined using Terraform.

The modules that define how the infrastructure should be created are in the [production Gitlab instance](https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/terraform/modules).
The `ops.gitlab.com` instance holds input data for these modules [here](https://ops.gitlab.net/opstrace-realm/environments/gcp).

## Multiple infrastructure environments

GOB infrastructure environments follow the [schema outlined for Gitlab](https://about.gitlab.com/handbook/engineering/infrastructure/environments/), except there are only three environment types currently in use:

* development
* staging
* production

## Google Cloud Platform (GCP)

All environments are deployed on GCP.

To learn more:

1. Go to [GCE webpage](https://cloud.google.com/).
1. Log in to the console.
1. Look for the `opstrace-dev-`, `opstrace-prd-`, and `opstrace-stg-` projects.

### Production environment

GOB runs on a Google Kubernetes Engine (GKE) cluster and connects to the `gitlab.com` production instance.

![Infrastructure - production](../assets/infra-production.png)

OAuth tokens are created manually by the GitLab instance administrator and passed to Terraform in the `terraform.tfvars` file.

The domain for the production environment is `observe.gitlab.com`

### Staging environment

GOB runs on a GKE cluster and connects to the `staging.gitlab.com` staging instance.

![Infrastructure - production](../assets/infra-staging.png)

OAuth tokens are created manually by the GitLab instance administrator and passed to Terraform in the `terraform.tfvars` file.

The domain for the staging environment is `observe.staging.gitlab.com`

### Development Environment

The development environment allows for launching multiple GOB clusters.
If needed, a separate GitLab instance can be launched for testing (for example, using the [Omnibus package](https://docs.gitlab.com/omnibus/)).

![Infrastructure - dev](../assets/infra-dev.png)

If a full stack is required, each cluster needs its own domain.
Refer to the wiki of the Project for details on how to create one.
There is no need to deploy every part of the stack, unless it is required for testing.
OAuth tokens are created manually by the GitLab instance administrator and passed to Terraform in the `terraform.tfvars` file.

## Other documentation

Every project in `ops.gitlab.com` includes its own wiki with some additional information on configuring:

* GCE credentials for deployment.
* DNS for projects.
* Terraform and kubectl access.

Additionally, each project contains a README.md file with further details.
