package tenant

import (
	"fmt"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type NamespaceReconciler struct {
	Teardown         bool
	Log              logr.Logger
	initialManifests map[string][]byte
}

func NewNamespaceReconciler(
	initialManifests map[string][]byte,
	teardown bool,
	tenantID int64,
) *NamespaceReconciler {
	return &NamespaceReconciler{
		Teardown: teardown,
		Log: logf.Log.WithName(
			fmt.Sprintf("tenantmanifests/%s-%d", "namespace", tenantID),
		),
		initialManifests: initialManifests,
	}
}

func (i *NamespaceReconciler) Reconcile(cr *v1alpha1.GitLabObservabilityTenant) common.DesiredState {
	if i.Teardown {
		// We are cleaning up, we'll just use the inventory to enlist things
		// to clean up
		return common.DesiredState{
			common.ManifestsPruneAction{
				ComponentName: constants.OtelCollectorNamespaceComponentName,
				GetInventory: func() *v1beta2.ResourceInventory {
					return cr.Status.Inventory[constants.GitLabObservabilityTenantNamespaceInventoryID]
				},
				Msg: "[tenantmanifests/namespace] prune",
				Log: i.Log,
			},
		}
	}

	overrideKustomization, err := i.ApplyConfiguration(cr)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/namespace] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}
	i.ApplyOverrides(overrideKustomization, cr)

	manifests, err := ctrlcommon.ApplyKustomization(i.initialManifests, overrideKustomization)
	if err != nil {
		return common.DesiredState{
			common.LogAction{
				Msg:   "[tenantmanifests/namespace] failed to apply configuration to manifests",
				Error: err,
			},
		}
	}

	return common.DesiredState{
		common.ManifestsEnsureAction{
			HealthcheckTimeout: 2 * time.Minute,
			Manifests:          manifests,
			ComponentName:      constants.OtelCollectorNamespaceComponentName,
			GetInventory: func() *v1beta2.ResourceInventory {
				return cr.Status.Inventory[constants.GitLabObservabilityTenantNamespaceInventoryID]
			},
			SetInventory: func(inv *v1beta2.ResourceInventory) {
				cr.Status.Inventory[constants.GitLabObservabilityTenantNamespaceInventoryID] = inv
			},
			Msg: "[tenantmanifests/namespace] ensure",
			Log: i.Log,
		},
	}
}

//nolint:unparam
func (i *NamespaceReconciler) ApplyConfiguration(
	cr *v1alpha1.GitLabObservabilityTenant,
) (*kustomize.Kustomization, error) {
	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
	}

	patches := []types.Patch{}
	// namespace
	patches = append(patches, types.Patch{
		Target: namespaceSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "replace", "path": "/metadata/name", "value": "%s"}
			]`,
			cr.Namespace(),
		),
	})
	res.Patches = patches

	return res, nil
}

func (i *NamespaceReconciler) ApplyOverrides(k *kustomize.Kustomization, cr *v1alpha1.GitLabObservabilityTenant) {
	if cr.Spec.Overrides.OTELCollector == nil {
		return
	}

	k.CommonLabels = cr.Spec.Overrides.OTELCollector.CommonLabels
	k.Labels = cr.Spec.Overrides.OTELCollector.Labels
	k.CommonAnnotations = cr.Spec.Overrides.OTELCollector.CommonAnnotations
	k.Patches = append(k.Patches, cr.Spec.Overrides.OTELCollector.Patches...)
	k.Images = cr.Spec.Overrides.OTELCollector.Images
	k.Replicas = cr.Spec.Overrides.OTELCollector.Replicas
}

func namespaceSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Namespace",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}
