package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type ClickHouseOperatorReconciler struct {
	BaseReconciler
}

func NewClickHouseOperatorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *ClickHouseOperatorReconciler {
	res := &ClickHouseOperatorReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.ClickHouseOperatorInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.ClickHouseOperatorInventoryID,
			reconcilerName: "clickhouse-operator",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.ClickHouseOperator
			},

			serviceAccountName: constants.ClickHouseOperatorServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *ClickHouseOperatorReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Images = []kustomize.Image{
		{
			Name:    "clickhouse-operator-image-placeholder",
			NewName: constants.DockerImageName(constants.ClickHouseImageName),
			NewTag:  constants.DockerImageTag,
		},
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.ClickHouseOperatorServiceMonitorName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s" }]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: constants.ClickHouseOperatorRoleBindingName,
				},
			},
		},
	)

	return res, nil
}
