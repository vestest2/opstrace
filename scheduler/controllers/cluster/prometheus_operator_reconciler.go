package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type PrometheusOperatorReconciler struct {
	BaseReconciler
}

func NewPrometheusOperatorReconciler(
	initialManifests map[string][]byte,
	teardown bool,
) *PrometheusOperatorReconciler {
	res := &PrometheusOperatorReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.PrometheusOperatorInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.PrometheusOperatorInventoryID,
			reconcilerName: "prometheus-operator",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.PrometheusOperator
			},

			serviceAccountName: constants.PrometheusOperatorServiceAccountName,
		},
	}

	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *PrometheusOperatorReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ServiceMonitor",
					},
					Name: constants.PrometheusOperatorServiceMonitorName,
				},
			},
		},
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/subjects/0/namespace", "value": "%s" }]`,
				cr.Namespace(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "ClusterRoleBinding",
					},
					Name: constants.PrometheusOperatorName,
				},
			},
		},
	)
	return res, nil
}
