#!/usr/bin/env bash

set -eou pipefail

# https://github.com/open-telemetry/opentelemetry-operator/releases
VER=v0.83.0
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

curl -sL \
    https://github.com/open-telemetry/opentelemetry-operator/releases/download/$VER/opentelemetry-operator.yaml \
    -o ${SCRIPT_DIR}/bundle.yaml

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
