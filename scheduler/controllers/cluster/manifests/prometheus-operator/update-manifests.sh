#!/usr/bin/env bash

set -eou pipefail

# https://github.com/prometheus-operator/prometheus-operator/releases
VER=v0.64.1
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
README_DIR=$(realpath $SCRIPT_DIR)

# We split the bundle in order to make it possible for tenant ang group tests
# to load crds into envtest.
curl -sL \
    https://github.com/prometheus-operator/prometheus-operator/releases/download/${VER}/bundle.yaml \
    -o ${SCRIPT_DIR}/bundle.yaml.tmp
yq e '. | select(.kind == "CustomResourceDefinition" and .metadata.name == "servicemonitors.monitoring.coreos.com" )' ${SCRIPT_DIR}/bundle.yaml.tmp > ${SCRIPT_DIR}/servicemonitors.crd.yaml
yq e '. | select(.kind != "CustomResourceDefinition" or .metadata.name != "servicemonitors.monitoring.coreos.com" )' ${SCRIPT_DIR}/bundle.yaml.tmp > ${SCRIPT_DIR}/bundle.yaml
rm ${SCRIPT_DIR}/bundle.yaml.tmp

echo "Please make sure to read the README.md file in the $README_DIR directory as well."
