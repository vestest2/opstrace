package persistentqueue

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type Item struct {
	Data string
}

func rng(limit int) int {
	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(limit)))
	if err != nil {
		panic(err)
	}
	return int(nBig.Int64())
}

func TestSimpleQueue(t *testing.T) {
	count := rng(1000)
	t.Logf("testing r/w with %d records\n", count)

	var readData []string
	var processor = func(buffer []interface{}) error {
		fmt.Printf("read a batch of %d records\n", len(buffer))
		for _, d := range buffer {
			item := fmt.Sprintf("%s", d)
			readData = append(readData, item)
			// fmt.Printf("read %v\n", item)
		}
		return nil
	}

	queueName := common.RandStringRunes(10)
	queue, err := NewQueue(
		WithDirPath(t.TempDir()),
		WithQueueName(queueName),
		WithBatchSize(100),
		WithProcessor(processor),
	)
	if err != nil {
		t.Fatal(err)
	}

	writtenData := make([]string, count)
	for i := 0; i < count; i++ {
		data := fmt.Sprintf("data_%d", i)
		writtenData[i] = data
		queue.Insert([]byte(data))
		// fmt.Printf("added %v\n", item.Data)
	}

	queue.Close()

	assert.Equal(
		t,
		len(writtenData),
		len(readData),
		"the two buffers are not equal (%d, %d), we are dropping data", len(writtenData), len(readData),
	)

	assert.ElementsMatch(
		t,
		readData,
		writtenData,
	)
}

func TestQueueWithMultipleProcessors(t *testing.T) {
	wLock := sync.Mutex{}
	readData := make(map[string]struct{})
	var processor = func(buffer []interface{}) error {
		fmt.Printf("read a batch of %d records\n", len(buffer))
		wLock.Lock()
		defer wLock.Unlock()
		for _, d := range buffer {
			item := fmt.Sprintf("%s", d)
			readData[item] = struct{}{}
			// fmt.Printf("read %v\n", item)
		}
		return nil
	}

	count := rng(1000)
	t.Logf("testing r/w with %d records\n", count)

	queueName := common.RandStringRunes(10)
	queue, err := NewQueue(
		WithDirPath(t.TempDir()),
		WithQueueName(queueName),
		WithBatchSize(100),
		WithProcessor(processor),
		WithProcessorCount(3),
	)
	if err != nil {
		t.Fatal(err)
	}

	writtenData := make(map[string]struct{})
	for i := 0; i < count; i++ {
		item := fmt.Sprintf("data_%d", i)
		writtenData[item] = struct{}{}
		queue.Insert([]byte(item))
		// fmt.Printf("added %v\n", item)
	}

	queue.Close()

	assert.Equal(
		t,
		len(writtenData),
		len(readData),
		"the two buffers are not equal (%d, %d), we are dropping data", len(writtenData), len(readData),
	)

	for k := range writtenData {
		if _, ok := readData[k]; !ok {
			t.Fatalf("written data not read back correctly, looked up %s", k)
		}
	}
}

func TestQueueWithRestarts(t *testing.T) {
	var readData []string
	var processor = func(buffer []interface{}) error {
		fmt.Printf("read a batch of %d records\n", len(buffer))
		for _, d := range buffer {
			item := fmt.Sprintf("%s", d)
			readData = append(readData, item)
			// fmt.Printf("read %v\n", item)
		}
		return nil
	}

	count := rng(1000)
	t.Logf("testing r/w with %d records\n", count)

	queueName := common.RandStringRunes(10)
	queue, err := NewQueue(
		WithDirPath(t.TempDir()),
		WithQueueName(queueName),
		WithBatchSize(100),
		WithProcessor(processor),
	)
	if err != nil {
		t.Fatal(err)
	}

	writtenData := make([]string, count)

	firstPush := count / 2
	for i := 0; i < firstPush; i++ {
		item := fmt.Sprintf("data_%d", i)
		writtenData[i] = item
		queue.Insert([]byte(item))
		// fmt.Printf("added %v\n", item.Data)
	}

	queue.Close()

	// re-build queue simulating a restart
	t.Logf("queue was restarted")
	queue, err = NewQueue(
		WithDirPath(t.TempDir()),
		WithQueueName(queueName),
		WithBatchSize(100),
		WithProcessor(processor),
	)
	if err != nil {
		t.Fatal(err)
	}

	// add data some more
	for i := firstPush; i < count; i++ {
		item := fmt.Sprintf("data_%d", i)
		writtenData[i] = item
		queue.Insert([]byte(item))
		// fmt.Printf("added %v\n", item)
	}

	queue.Close()

	assert.Equal(
		t,
		len(writtenData),
		len(readData),
		"the two buffers are not equal (%d, %d), we are dropping data", len(writtenData), len(readData),
	)

	assert.ElementsMatch(
		t,
		readData,
		writtenData,
	)
}
