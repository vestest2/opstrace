package common

import (
	"go.opentelemetry.io/collector/pdata/pcommon"
)

const (
	ProjectIDUnknown = "unknown"
	ProjectIDHeader  = "gitlab.target_project_id"
)

func GetProjectID(attributes map[string]string) string {
	if _, ok := attributes[ProjectIDHeader]; ok {
		return attributes[ProjectIDHeader]
	}
	return ProjectIDUnknown
}

func AttributesToMap(attributes pcommon.Map) map[string]string {
	m := make(map[string]string, attributes.Len())
	attributes.Range(func(k string, v pcommon.Value) bool {
		m[k] = v.AsString()
		return true
	})
	return m
}
