package gitlabobservabilityexporter

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/pdata/ptrace"
	conventions "go.opentelemetry.io/collector/semconv/v1.18.0"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
)

type tracesExporter struct {
	db     *database
	logger *zap.Logger
	cfg    *config

	cloudDB *database
}

func newTracesExporter(logger *zap.Logger, cfg *config) (*tracesExporter, error) {
	db, err := newDB(cfg)
	if err != nil {
		return nil, err
	}
	cloudDB, err := newCloudDB(cfg)
	if err != nil {
		return nil, err
	}
	return &tracesExporter{
		db:      db,
		logger:  logger,
		cfg:     cfg,
		cloudDB: cloudDB,
	}, nil
}

func (e *tracesExporter) start(ctx context.Context, _ component.Host) error {
	pingCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	if err := e.db.conn.Ping(pingCtx); err != nil {
		return fmt.Errorf("pinging clickhouse backend: %w", err)
	}

	if e.cloudDB != nil {
		pingCtx, cancel = context.WithTimeout(ctx, 30*time.Second)
		defer cancel()
		if err := e.cloudDB.conn.Ping(pingCtx); err != nil {
			return fmt.Errorf("pinging clickhouse cloud backend: %w", err)
		}
	}
	return nil
}

func (e *tracesExporter) shutdown(_ context.Context) error {
	if e.db != nil {
		if e.db.conn != nil {
			if err := e.db.conn.Close(); err != nil {
				return fmt.Errorf("closing underlying connection: %w", err)
			}
		}
	}

	if e.cloudDB != nil {
		if e.cloudDB.conn != nil {
			if err := e.cloudDB.conn.Close(); err != nil {
				return fmt.Errorf("closing cloud db conn: %w", err)
			}
		}
	}
	return nil
}

const (
	insertSQL = `
INSERT INTO gl_traces_main (
	ProjectId,
	Timestamp,
	TraceId,
	SpanId,
	ParentSpanId,
	TraceState,
	SpanName,
	SpanKind,
	ServiceName,
	ResourceAttributes,
	ScopeName,
	ScopeVersion,
	SpanAttributes,
	Duration,
	StatusCode,
	StatusMessage,
	Events.Timestamp,
	Events.Name,
	Events.Attributes,
	Links.TraceId,
	Links.SpanId,
	Links.TraceState,
	Links.Attributes
)`
)

//nolint:funlen
func (e *tracesExporter) pushTraceData(ctx context.Context, td ptrace.Traces) error {
	m := &ptrace.ProtoMarshaler{}
	tracedataSizeBytes.WithLabelValues(e.cfg.TenantID).Add(float64(m.TracesSize(td)))

	batch, err := e.db.conn.PrepareBatch(ctx, insertSQL)
	if err != nil {
		return fmt.Errorf("preparing traces batch for clickhouse backend: %w", err)
	}
	var batchSize float64

	var anotherBatch driver.Batch
	if e.cloudDB != nil {
		anotherBatch, err = e.cloudDB.conn.PrepareBatch(ctx, insertSQL)
		if err != nil {
			return fmt.Errorf("preparing traces batch for clickhouse cloud: %w", err)
		}
	}

	for i := 0; i < td.ResourceSpans().Len(); i++ {
		spans := td.ResourceSpans().At(i)
		res := spans.Resource()
		resAttr := common.AttributesToMap(res.Attributes())

		var serviceName string
		if v, ok := res.Attributes().Get(conventions.AttributeServiceName); ok {
			serviceName = v.Str()
		}

		for j := 0; j < spans.ScopeSpans().Len(); j++ {
			rs := spans.ScopeSpans().At(j).Spans()
			scopeName := spans.ScopeSpans().At(j).Scope().Name()
			scopeVersion := spans.ScopeSpans().At(j).Scope().Version()

			spansReceivedCounter.WithLabelValues(e.cfg.TenantID).Add(float64(rs.Len()))
			for k := 0; k < rs.Len(); k++ {
				r := rs.At(k)
				status := r.Status()

				eventTimes, eventNames, eventAttrs := convertEvents(r.Events())
				linksTraceIDs, linksSpanIDs, linksTraceStates, linksAttrs := convertLinks(r.Links())

				spanAttr := common.AttributesToMap(r.Attributes())
				projectID := common.GetProjectID(spanAttr)
				delete(spanAttr, common.ProjectIDHeader) // delete projectID header before persisting

				if err := batch.Append(
					projectID,
					r.StartTimestamp().AsTime(),
					TraceIDToFixedString(r.TraceID()),
					SpanIDToFixedString(r.SpanID()),
					SpanIDToFixedString(r.ParentSpanID()),
					r.TraceState().AsRaw(),
					r.Name(),
					SpanKindStr(r.Kind()),
					serviceName,
					resAttr,
					scopeName,
					scopeVersion,
					spanAttr,
					r.EndTimestamp().AsTime().Sub(r.StartTimestamp().AsTime()).Nanoseconds(),
					StatusCodeStr(status.Code()),
					status.Message(),
					eventTimes,
					eventNames,
					eventAttrs,
					linksTraceIDs,
					linksSpanIDs,
					linksTraceStates,
					linksAttrs,
				); err != nil {
					e.logger.Debug("appending span entry", zap.Error(err))
					continue
				}
				batchSize++

				if anotherBatch != nil {
					if err := anotherBatch.Append(
						projectID,
						r.StartTimestamp().AsTime(),
						TraceIDToFixedString(r.TraceID()),
						SpanIDToFixedString(r.SpanID()),
						SpanIDToFixedString(r.ParentSpanID()),
						r.TraceState().AsRaw(),
						r.Name(),
						SpanKindStr(r.Kind()),
						serviceName,
						resAttr,
						scopeName,
						scopeVersion,
						spanAttr,
						r.EndTimestamp().AsTime().Sub(r.StartTimestamp().AsTime()).Nanoseconds(),
						StatusCodeStr(status.Code()),
						status.Message(),
						eventTimes,
						eventNames,
						eventAttrs,
						linksTraceIDs,
						linksSpanIDs,
						linksTraceStates,
						linksAttrs,
					); err != nil {
						e.logger.Debug("appending span entry to cloud conn", zap.Error(err))
						continue
					}
				}
			}
		}
	}

	if err := batch.Send(); err != nil {
		return fmt.Errorf("sending traces batch to clickhouse backend: %w", err)
	}
	spansIngestedCounter.WithLabelValues(e.cfg.TenantID).Add(batchSize)

	if anotherBatch != nil {
		if err := anotherBatch.Send(); err != nil {
			return fmt.Errorf("sending traces batch to clickhouse cloud backend: %w", err)
		}
	}
	return nil
}

func convertEvents(events ptrace.SpanEventSlice) ([]time.Time, []string, []map[string]string) {
	var (
		times []time.Time
		names []string
		attrs []map[string]string
	)
	for i := 0; i < events.Len(); i++ {
		event := events.At(i)
		times = append(times, event.Timestamp().AsTime())
		names = append(names, event.Name())
		attrs = append(attrs, common.AttributesToMap(event.Attributes()))
	}
	return times, names, attrs
}

func convertLinks(links ptrace.SpanLinkSlice) ([]string, []string, []string, []map[string]string) {
	var (
		traceIDs []string
		spanIDs  []string
		states   []string
		attrs    []map[string]string
	)
	for i := 0; i < links.Len(); i++ {
		link := links.At(i)
		traceIDs = append(traceIDs, TraceIDToFixedString(link.TraceID()))
		spanIDs = append(spanIDs, SpanIDToFixedString(link.SpanID()))
		states = append(states, link.TraceState().AsRaw())
		attrs = append(attrs, common.AttributesToMap(link.Attributes()))
	}
	return traceIDs, spanIDs, states, attrs
}
