package types

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

var tests = []struct {
	payload    []byte
	filepath   string
	sdkName    string
	sdkVersion string
}{
	{
		payload: []byte(`{"event_id":"9ec79c33ec9942ab8353589fcb2e04dc","dsn":"https://e12d836b15bb49d7bbf99e64295d995b:@sentry.io/42"}
{"type":"attachment","length":10,"content_type":"text/plain","filename":"hello.txt"}
\xef\xbb\xbfHello\r\n
{"type":"event","length":41,"content_type":"application/json","filename":"application.log"}
{"message":"hello world","level":"error"}`),
	},
	{
		payload: []byte("{\"event_id\":\"9ec79c33ec9942ab8353589fcb2e04dc\"}\n{\"type\":\"attachment\",\"length\":0}\n\n{\"type\":\"attachment\",\"length\":0}\n\n"),
	},
	{
		payload: []byte("{\"event_id\":\"9ec79c33ec9942ab8353589fcb2e04dc\"}\n{\"type\":\"attachment\"}\nhelloworld"),
	},
	{
		payload: []byte(`{}
{"type":"session"}
{"started": "2020-02-07T14:16:00Z","attrs":{"release":"sentry-test@1.0.0"}}`),
	},
	{
		payload: []byte("{\"event_id\":\"6871f9e0-8c2f-4f4c-925b-2ca79fe623c4\"}\n{\"type\":\"event\",\"length\":4788}\n{\"event_id\":\"6871f9e08c2f4f4c925b2ca79fe623c4\",\"level\":\"fatal\",\"platform\":\"native\",\"timestamp\":1681225569.186447,\"server_name\":\"192.168.1.152\",\"release\":\"test-sentry-project@0.1.0\",\"environment\":\"development\",\"contexts\":{\"device\":{\"type\":\"device\",\"family\":\"MacBookPro\",\"model\":\"MacBookPro18,2\",\"arch\":\"aarch64\"},\"os\":{\"type\":\"os\",\"name\":\"Darwin\",\"version\":\"22.4.0\",\"kernel_version\":\"Darwin Kernel Version 22.4.0: Mon Mar  6 20:59:28 PST 2023; root:xnu-8796.101.5~3/RELEASE_ARM64_T6000\"},\"rust\":{\"type\":\"runtime\",\"name\":\"rustc\",\"version\":\"1.65.0\",\"channel\":\"stable\"}},\"exception\":{\"values\":[{\"type\":\"panic\",\"value\":\"test panic\",\"stacktrace\":{\"frames\":[{\"function\":\"_main\",\"in_app\":true,\"instruction_addr\":\"0x10213fc98\"},{\"function\":\"std::rt::lang_start\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":165,\"in_app\":false,\"instruction_addr\":\"0x102140994\"},{\"function\":\"std::rt::lang_start_internal\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":148,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panic::catch_unwind\",\"package\":\"std\",\"filename\":\"panic.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panic.rs\",\"lineno\":137,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":456,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try::do_call\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":492,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::rt::lang_start_internal::{{closure}}\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":148,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panic::catch_unwind\",\"package\":\"std\",\"filename\":\"panic.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panic.rs\",\"lineno\":137,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":456,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try::do_call\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":492,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once\",\"package\":\"core\",\"filename\":\"function.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/core/src/ops/function.rs\",\"lineno\":283,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::rt::lang_start::{{closure}}\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":166,\"in_app\":false,\"instruction_addr\":\"0x1021409cc\"},{\"function\":\"std::sys_common::backtrace::__rust_begin_short_backtrace\",\"package\":\"std\",\"filename\":\"backtrace.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/sys_common/backtrace.rs\",\"lineno\":122,\"in_app\":false,\"instruction_addr\":\"0x102142128\"},{\"function\":\"core::ops::function::FnOnce::call_once\",\"package\":\"core\",\"filename\":\"function.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/core/src/ops/function.rs\",\"lineno\":248,\"in_app\":false,\"instruction_addr\":\"0x102140a70\"},{\"function\":\"test_sentry_project::main\",\"package\":\"test_sentry_project\",\"filename\":\"main.rs\",\"abs_path\":\"/Users/drossetti/Developer/test-sentry-project/src/main.rs\",\"lineno\":36,\"in_app\":true,\"instruction_addr\":\"0x10213fc6c\"},{\"function\":\"std::panicking::begin_panic\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":616,\"in_app\":false,\"instruction_addr\":\"0x1028a3c4c\"},{\"function\":\"std::sys_common::backtrace::__rust_end_short_backtrace\",\"package\":\"std\",\"filename\":\"backtrace.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/sys_common/backtrace.rs\",\"lineno\":138,\"in_app\":false,\"instruction_addr\":\"0x1025dec7c\"}]},\"mechanism\":{\"type\":\"panic\",\"handled\":false}}]},\"sdk\":{\"name\":\"sentry.rust\",\"version\":\"0.23.0\",\"integrations\":[\"attach-stacktrace\",\"contexts\",\"panic\",\"process-stacktrace\"],\"packages\":[{\"name\":\"cargo:sentry\",\"version\":\"0.23.0\"}]}}\n"),
	},
	{
		filepath:   "../testdata/exceptions/java-sdk-envelope.txt",
		sdkName:    "sentry.java",
		sdkVersion: "6.3.0",
	},
	{
		filepath:   "../testdata/exceptions/javascript-sdk-envelope.txt",
		sdkName:    "sentry.javascript.node",
		sdkVersion: "7.2.0",
	},
	{
		filepath:   "../testdata/message/ruby_message_envelope.txt",
		sdkName:    "sentry.ruby",
		sdkVersion: "5.4.1",
	},
	{
		filepath:   "../testdata/transactions/node-envelope.txt",
		sdkName:    "sentry.javascript.node",
		sdkVersion: "7.7.0",
	},
}

func TestNewEnvelopeFrom(t *testing.T) {
	assert := assert.New(t)
	for _, test := range tests {
		var (
			payload []byte
			err     error
		)
		// if filepath is specified, read payload first
		if test.filepath != "" {
			payload, err = os.ReadFile(test.filepath)
			assert.NotEmpty(payload)
			assert.Nil(err)
		} else {
			payload = test.payload
		}

		envelope, err := NewEnvelopeFrom(payload)
		assert.NotNil(envelope)
		assert.Nil(err)

		assert.NotNil(envelope.Headers)
		if test.sdkName != "" || test.sdkVersion != "" {
			assert.NotNil(envelope.Headers.SDK)
			// test required attributes
			assert.NotEmpty(envelope.Headers.SDK.Name)
			assert.Equal(test.sdkName, envelope.Headers.SDK.Name)
			assert.NotEmpty(envelope.Headers.SDK.Version)
			assert.Equal(test.sdkVersion, envelope.Headers.SDK.Version)
		}

		for _, item := range envelope.Items {
			// t.Logf("%s\n%s\n", item.Type.Type, item.Payload)
			switch item.Type.Type {
			case "event":
				event, err := NewEventFrom(item.Payload)
				assert.NotNil(event)
				assert.Nil(err)
			}
		}
	}
}
