package models

import (
	"fmt"
	"time"

	"github.com/OneOfOne/xxhash"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

// ErrorTrackingErrorEvent maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingErrorEvent struct {
	ProjectID      uint64    `ch:"project_id"`
	Fingerprint    uint32    `ch:"fingerprint"`
	Name           string    `ch:"name"`
	Description    string    `ch:"description"`
	Actor          string    `ch:"actor"`
	Environment    string    `ch:"environment"`
	Platform       string    `ch:"platform"`
	Level          string    `ch:"level"`
	UserIdentifier string    `ch:"user_identifier"`
	Payload        string    `ch:"payload"`
	OccurredAt     time.Time `ch:"occurred_at"`
}

// NewErrorTrackingErrorEvent is a helper that returns a ErrorTrackingErrorEvent
// from the given parameters and calculates a fingerprint using a 64-bit xxHash
// algorithm form the error event name, actor and platform.
func NewErrorTrackingErrorEvent(projectID uint64, e *types.Event, payload []byte) *ErrorTrackingErrorEvent {
	// Choose a constant seed as we need same fingerprints for same underlying content
	hash := xxhash.NewS32(100)
	// WriteString never returns an error
	//nolint:errcheck
	hash.WriteString(fmt.Sprintf("%s|%s|%s", e.Name(), e.ExceptionActor(), e.Platform))
	fingerprint32 := hash.Sum32()

	return &ErrorTrackingErrorEvent{
		ProjectID:   projectID,
		Name:        e.Name(),
		Description: e.Description(),
		Actor:       e.ExceptionActor(),
		Platform:    e.Platform,
		OccurredAt:  e.Timestamp,
		Environment: e.Environment,
		Level:       e.Level,
		Payload:     string(payload),
		Fingerprint: fingerprint32,
	}
}

func (e ErrorTrackingErrorEvent) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO gl_error_tracking_error_events VALUES (%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
		e.ProjectID,
		e.Fingerprint,
		quote(e.Name),
		quote(e.Description),
		quote(e.Actor),
		quote(e.Environment),
		quote(e.Platform),
		quote(e.Level),
		quote(e.UserIdentifier),
		quote(e.Payload),
		formatTime(e.OccurredAt, tz),
	)
}
