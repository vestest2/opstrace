package models

import "time"

// ErrorTrackingError maps to the corresponding table in the clickhouse database.
type ErrorTrackingError struct {
	ProjectID             uint64    `ch:"project_id"`
	Fingerprint           uint32    `ch:"fingerprint"`
	Name                  string    `ch:"name"`
	Description           string    `ch:"description"`
	Actor                 string    `ch:"actor"`
	EventCount            uint64    `ch:"event_count"`
	ApproximatedUserCount uint64    `ch:"approximated_user_count"`
	LastSeenAt            time.Time `ch:"last_seen_at"`
	FirstSeenAt           time.Time `ch:"first_seen_at"`
	Status                uint8     `ch:"status"`
	Ignored               bool      `ch:"ignored"`
}
