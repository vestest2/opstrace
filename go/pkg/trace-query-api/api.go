package query

import (
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/telemetry"
)

func SetRoutes(controller *Controller, router *gin.Engine) {
	router.Use(errorLogger(controller.Logger))
	router.Use(telemetry.InstrumentHandlerFuncWithOpts(stripProjectIDFromPath, telemetry.NoOpAdditionalMetricsFunc))

	// This route will be removed once clients (GitLab UI) have migrated
	// to the v1 route below
	query := router.Group("/query/:group_id/:project_id")
	{
		v1 := query.Group("/v1")
		{
			v1.GET("/traces", controller.TraceHandlerFactory(HandlerFactoryOptions{version: NoVersion}))
		}
	}
	// New route that only requires :project_id
	// Also prefixes with version to align with versioning of all other APIs
	v3 := router.Group("/v3")
	{
		query := v3.Group("/query/:project_id")
		{
			query.GET("/traces", controller.TraceHandlerFactory(HandlerFactoryOptions{version: V3}))
			query.GET("/services", controller.ServicesHandler)
			query.GET("/services/:service_name/operations", controller.ServiceOperationsHandler)
		}
	}
}

type Controller struct {
	Q      QueryDB
	Logger *zap.Logger
}
