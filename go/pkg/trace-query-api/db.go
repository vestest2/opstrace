package query

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"go.uber.org/zap"
)

type QueryDB interface {
	GetTraces(ctx context.Context, projectID int64, filter *requestFilterParams) ([]*TraceItem, string, error)
	// GetServices returns span service names associated with a project.
	GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error)
	// GetServiceOperations returns span operation names associated with a project and service.
	GetServiceOperations(ctx context.Context, projectID int64, serviceName string) ([]OperationItem, error)
	Close() error
}

type querier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

func NewQuerier(clickHouseDsn string, opts *clickhouse.Options, logger *zap.Logger) (QueryDB, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	if opts != nil {
		dbOpts.MaxOpenConns = opts.MaxOpenConns
		dbOpts.MaxIdleConns = opts.MaxIdleConns
	}
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}
	if logger.Level() == zap.DebugLevel {
		dbOpts.Debug = true
	}

	db, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("open DB :%w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err = db.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("connecting to clickhouse: %w", err)
	}
	return &querier{db: db, logger: logger}, nil
}

//nolint:lll
const traceMVQueryTmpl = `
WITH
    %s(toDateTime(?)) AS r_start,
    %s(toDateTime(?)) AS r_end
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = ?)`

//nolint:lll
const traceIDMVQueryTmpl = `
SELECT
    UUIDNumToString(TraceId) AS traceID,
    min(Start) as trace_start_ts,
    groupArray(
        (hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
    ) AS spans,
    length(spans) AS total_spans
    FROM gl_traces_rolled
    WHERE (ProjectId = ?)`

const traceMVQueryGroup = `
    GROUP BY TraceId
`

const traceMVQueryPage = `
    HAVING trace_start_ts < ?
`

const traceMVQuery = `
    ORDER BY trace_start_ts DESC
    LIMIT %d
`

const defaultPageSize = 100

func buildTraceMVQuery(projectID int64, filter *requestFilterParams, refTime time.Time) *queryBuilder {
	builder := &queryBuilder{}

	// special case if traceID is provided as a filter but no period params
	if filter.Period == "" && len(filter.TraceIDs) > 0 {
		traceQuery := traceIDMVQueryTmpl
		builder.build(traceQuery, strconv.FormatInt(projectID, 10))
	} else {
		startTime, endTime, _, timeFunc := common.InferTimelines(refTime, filter.Period)
		traceQuery := fmt.Sprintf(traceMVQueryTmpl, timeFunc, timeFunc)
		builder.build(
			traceQuery,
			startTime,
			endTime, strconv.FormatInt(projectID, 10))
	}

	addFilter(builder, filter)
	builder.build(traceMVQueryGroup)

	if filter.cursor != nil && filter.cursor.StartTimestamp != "" {
		builder.build(traceMVQueryPage, filter.cursor.StartTimestamp)
	}

	pageSize := int64(defaultPageSize)
	if filter.Page != nil && filter.Page.PageSize > 0 {
		pageSize = filter.Page.PageSize
	}
	builder.build(fmt.Sprintf(traceMVQuery, pageSize))
	return builder
}

type DBItem struct {
	TraceID      string    `ch:"traceID" json:"trace_id"`
	TraceStartTS time.Time `ch:"trace_start_ts"`
	TotalSpans   uint64    `ch:"total_spans"`
	Values       [][]any   `ch:"spans"`
}

func (q *querier) GetTraces(
	ctx context.Context,
	projectID int64,
	filter *requestFilterParams) (results []*TraceItem, nextPageToken string, err error) {
	q.logger.Debug("got filter", zap.Any("filters", filter))
	results = make([]*TraceItem, 0)

	builder := buildTraceMVQuery(projectID, filter, time.Now().UTC())
	rows, err := q.db.Query(ctx, builder.sql, builder.args...)
	if err != nil {
		return nil, "", fmt.Errorf("query traces: %w", err)
	}

	for rows.Next() {
		item := &DBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, "", fmt.Errorf("scan results from clickhouse: %w", err)
		}

		trace := TraceItem{}
		trace.TraceID = item.TraceID
		trace.TotalSpans = item.TotalSpans
		trace.Timestamp = item.TraceStartTS

		//nolint:errcheck
		for _, i := range item.Values {
			// TODO(Arun): Investigate if we can improve marshaling strategy.
			// This is a straight pass from DB results and should be fastest.
			spanItem := SpanItem{}
			spanItem.SpanID = i[0].(string)
			spanItem.Timestamp = i[1].(time.Time)
			spanItem.ServiceName = i[2].(string)
			spanItem.Operation = i[3].(string)
			spanItem.StatusCode = i[4].(string)
			spanItem.DurationNano = uint64(i[5].(int64))
			spanItem.ParentSpanID = i[6].(string)
			spanItem.TraceID = item.TraceID

			if len(trace.Spans) == 0 {
				// The first span will always be root span as they are ordered by start timestamp.
				trace.ServiceName = spanItem.ServiceName
				trace.Operation = spanItem.Operation
				trace.StatusCode = spanItem.StatusCode
				trace.Spans = append(trace.Spans, spanItem)
				// Estimate duration of the trace from the root span.
				// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2274.
				trace.DurationNano = spanItem.DurationNano
			} else {
				trace.Spans = append(trace.Spans, spanItem)
			}
		}

		results = append(results, &trace)
	}

	//nolint:lll
	// The logic to pick up the next cursor is described below.
	// The SQL query looks like this:
	//	WITH
	//	toStartOfHour(toDateTime(1694438312)) AS r_start,
	//	toStartOfHour(toDateTime(1694528312)) AS r_end
	//	SELECT
	//	UUIDNumToString(TraceId) AS traceID,
	//		min(Start) as trace_start_ts,
	//		groupArray(
	//			(hex(SpanId) AS spanID, Start AS start, ServiceName AS serviceName, SpanName AS operation, StatusCode AS statusCode, Duration AS duration, if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID)
	// ) AS spans,
	//		length(spans) AS total_spans
	//	FROM gl_traces_rolled
	//	WHERE (Start > r_start) AND (Start < r_end) AND (ProjectId = '123')
	//	GROUP BY TraceId
	//	ORDER BY trace_start_ts DESC
	//	LIMIT $PAGE_SIZE
	// The results returned from the query are sorted descending when returned.
	// MV as defined will keep the ASC order of included spans to enforce the root span and child span relationship.
	// Taking an example with span timestamps:
	// Full result of 5 total items without pagination can be : [ {t5, ..}, {t4, ..}, {t3, ..}, {t2, ..}, {t1, ..} ]
	// After enforcing page size of 3, the result is : [ {t5, ..}, {t4, ..}, {t3, ..} ]
	// So in order to pick the next page, the cursor should be set to choose items earlier than t3
	// i.e. 'HAVING trace_start_ts < results[-1].Timestamp'
	// The next result will be:  [ {t2 ..}, {t1, ..} ].

	// Note(Arun): Currently there is no indication if there are no more result rows.
	// So we always return a next page token in responses. Clients will see no results
	// after latest rows were returned in previous response.
	if n := len(results); n > 0 {
		nextPage := &Page{
			StartTimestamp: results[n-1].Timestamp.Format("2006-01-02 15:04:05.000000000"),
		}
		nextPageToken, err = encodePage(nextPage)
		if err != nil {
			return nil, "", fmt.Errorf("encoding page cursor: %w", err)
		}
	}

	return results, nextPageToken, nil
}

func (q *querier) Close() error {
	err := q.db.Close()
	if err != nil {
		return fmt.Errorf("close DB: %w", err)
	}
	return nil
}

func addFilter(b *queryBuilder, filter *requestFilterParams) {
	if filter.TraceIDs != nil {
		//nolint:lll
		// TODO(Arun): This does not work as expected because of a bug in ClickHouse https://github.com/ClickHouse/ClickHouse/issues/14980
		// b.build(" AND ( TraceId IN arrayMap(x -> UUIDStringToNum(x), ? ) ) ", filter.TraceIDs)

		// Workaround to the above transformation to wrap the list of traceIDs inside the conversion function
		// on the client side. This has the same effect of the above query and uses the primary index.

		transformed := make([]interface{}, len(filter.TraceIDs))
		for i, v := range filter.TraceIDs {
			transformed[i] = v
		}
		// TrimRight is to remove an extra "," at the end of the string.
		appliedFunc := strings.TrimRight(strings.Repeat(" UUIDStringToNum(?),", len(transformed)), ",")

		// Note that we pass each string as a separate bind parameter instead of binding the whole array as
		// parameter. This is to avoid quoting the conversion function itself.
		b.build(" AND ( TraceId IN ["+appliedFunc+" ] ) ", transformed...)
	}

	if filter.ServiceNames != nil {
		b.build(" AND ( ServiceName IN ( ? ) ) ", filter.ServiceNames)
	}
	if filter.NotServiceNames != nil {
		b.build(" AND ( ServiceName NOT IN ( ? ) ) ", filter.NotServiceNames)
	}

	if filter.Operations != nil {
		b.build(" AND ( SpanName IN ( ? ) ) ", filter.Operations)
	}
	if filter.NotOperations != nil {
		b.build(" AND ( SpanName NOT IN ( ? ) ) ", filter.NotOperations)
	}

	if filter.LtDuration != 0 {
		b.build(" AND ( Duration <= ? ) ", filter.LtDuration)
	}
	if filter.GtDuration != 0 {
		b.build(" AND ( Duration >= ? ) ", filter.GtDuration)
	}
}
