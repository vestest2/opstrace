package query

import (
	"context"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type mockQueryDB struct {
	passedFilter *requestFilterParams
	err          error
}

func (m *mockQueryDB) Close() error {
	return nil
}

func (m *mockQueryDB) GetTraces(ctx context.Context, projectID int64, filter *requestFilterParams) ([]*TraceItem, string, error) {
	m.passedFilter = filter
	ts := time.Now().UTC()
	traceID := uuid.New().String()
	return []*TraceItem{
		{
			Timestamp:   ts,
			TraceID:     traceID,
			ServiceName: "sample-service",
			Operation:   "sample-operation",
			StatusCode:  "OK",
			Spans: []SpanItem{
				{
					Timestamp:    ts,
					SpanID:       uuid.New().String(),
					TraceID:      traceID,
					ServiceName:  "sample-service",
					Operation:    "sample-operation",
					DurationNano: 10000,
					ParentSpanID: "",
					StatusCode:   "OK",
				},
				{
					Timestamp:    ts.Add(time.Millisecond * 10),
					SpanID:       uuid.New().String(),
					TraceID:      traceID,
					ServiceName:  "sample-service",
					Operation:    "sample-operation",
					DurationNano: 1000,
					StatusCode:   "OK",
				},
			},
			TotalSpans: 2,
		},
	}, "", nil
}

func (m *mockQueryDB) GetServices(ctx context.Context, projectID int64) ([]ServiceItem, error) {
	if m.err != nil {
		return nil, m.err
	}
	return []ServiceItem{
		{
			Name: "sample-service",
		},
	}, nil
}

func (m *mockQueryDB) GetServiceOperations(ctx context.Context, projectID int64, serviceName string) ([]OperationItem, error) {
	if m.err != nil {
		return nil, m.err
	}
	return []OperationItem{
		{
			Name: "sample-operation",
		},
	}, nil
}

var (
	logger *zap.SugaredLogger
)

func TestTraceQueryAPI(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Trace Query API Test suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core)
	log = log.WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = GinkgoWriter
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
