package common

import (
	"time"
)

const (
	Period1m  = "1m"  // 1 minute
	Period5m  = "5m"  // 5 minutes
	Period15m = "15m" // 15 minutes
	Period30m = "30m" // 30 minutes
	Period1h  = "1h"  // 1 hour
	Period4h  = "4h"  // 4 hours
	Period12h = "12h" // 12 hours
	Period24h = "24h" // 24 hours
	Period7d  = "7d"  // 7 days
	Period14d = "14d" // 14 days
	Period30d = "30d" // 30 days
)

const (
	nrSecondsMinute = 60
	nrSecondsHour   = 60 * 60
	nrSecondsDay    = 60 * 60 * 24
)

// ClickHouse-specific date-time function-names.
// See: https://clickhouse.com/docs/en/sql-reference/functions/date-time-functions
const (
	timeFuncStartMinute string = "toStartOfMinute"
	timeFuncStartHour   string = "toStartOfHour"
	timeFuncStartDay    string = "toStartOfDay"
)

//nolint:funlen
func InferTimelines(refTime time.Time, statsPeriod string) (int64, int64, int64, string) {
	var (
		delta       time.Duration
		startTime   int64
		endTime     int64
		stepSeconds int64
		timeFunc    string
	)
	switch statsPeriod {
	case Period1m:
		delta = 1 * time.Minute
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period5m:
		delta = 5 * time.Minute
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period15m:
		delta = 15 * time.Minute
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period30m:
		delta = 30 * time.Minute
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period1h:
		delta = time.Hour
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period4h:
		delta = 4 * time.Hour
		endTime = refTime.Add(time.Minute).Unix()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period12h:
		delta = 12 * time.Hour
		endTime = refTime.Add(time.Hour).Unix()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	case Period24h:
		delta = 24 * time.Hour
		endTime = refTime.Add(time.Hour).Unix()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	case Period7d:
		delta = 7 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).Unix()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	case Period14d:
		delta = 14 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).Unix()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	case Period30d:
		delta = 30 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).Unix()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	default: // to 24h
		delta = 24 * time.Hour
		endTime = refTime.Add(time.Hour).Unix()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	}

	startTime = refTime.Add(-1 * delta).Unix()
	return startTime, endTime, stepSeconds, timeFunc
}
