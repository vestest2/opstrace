provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

# GCS-backed disk creation on CH does not support authenticating
# with service account HMAC keys. For now, we resort to creating
# user account HMAC keys out of band and passing it to this module.

# Create a new service account
resource "google_service_account" "service_account" {
  account_id = var.service_account_name
}

# Create the HMAC key for the associated service account
resource "google_storage_hmac_key" "key" {
  service_account_email = google_service_account.service_account.email
}


# Create the GCS bucket
resource "google_storage_bucket" "clickhouse_data" {
  name          = var.bucket_name
  location      = var.bucket_location
  storage_class = var.storage_class

  force_destroy = false

  public_access_prevention = "enforced"

  versioning {
    enabled = true
  }
  // Enforce lifecycle rules of keeping 1 non-current version and deleting non-current objects after 7 days.
  // This is the default configuration that comes while creating the bucket via Console.
  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      num_newer_versions = 1
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      with_state = "ARCHIVED"
    }
  }

  // Enforce dual region replication.
  // In order to aid performance between ClickHouse and GCS, it is essential to have the bucket in the same region and zone
  // as the ClickHouse nodes.
  // Choose the other region that has the lowest latency from the default region.
  // Reasons for not using default multi region(s) is to ensure that we keep the buckets in the nearest region
  // and keep the option to enable turbo replication if required.
  custom_placement_config {
    data_locations = var.bucket_regions
  }
}

resource "google_storage_bucket_iam_member" "member" {
  bucket     = google_storage_bucket.clickhouse_data.name
  role       = "roles/storage.objectAdmin"
  member     = google_service_account.service_account.member
  depends_on = [google_storage_bucket.clickhouse_data, google_service_account.service_account]
}

resource "google_service_account" "backup_service_account" {
  count      = var.provision_backup_bucket ? 1 : 0
  account_id = var.backup_service_account_name
}

resource "google_storage_hmac_key" "backup_key" {
  count                 = var.provision_backup_bucket ? 1 : 0
  service_account_email = google_service_account.backup_service_account[0].email
}

locals {
  backup_service_account_hmac_access_id = one(google_storage_hmac_key.backup_key[*].access_id)
  backup_service_account_hmac_secret    = one(google_storage_hmac_key.backup_key[*].secret)
  backup_bucket_name                    = one(google_storage_bucket.clickhouse_backup[*].name)
}

resource "google_storage_bucket" "clickhouse_backup" {
  count         = var.provision_backup_bucket ? 1 : 0
  name          = var.backup_bucket_name
  location      = var.backup_bucket_location
  storage_class = var.backup_bucket_storage_class

  force_destroy = false

  public_access_prevention = "enforced"

  versioning {
    enabled = true
  }
  // Enforce lifecycle rules of keeping 1 non-current version and deleting non-current objects after 7 days.
  // This is the default configuration that comes while creating the bucket via Console.
  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      num_newer_versions = 1
    }
  }

  lifecycle_rule {
    action {
      type = "Delete"
    }
    condition {
      with_state = "ARCHIVED"
    }
  }

}

resource "google_storage_bucket_iam_member" "backup_member" {
  count      = var.provision_backup_bucket ? 1 : 0
  bucket     = google_storage_bucket.clickhouse_backup[0].name
  role       = "roles/storage.objectAdmin"
  member     = google_service_account.backup_service_account[0].member
  depends_on = [google_storage_bucket.clickhouse_backup, google_service_account.backup_service_account]
}

// Also grant objectUser role on the storage bucket in order to access objects
resource "google_storage_bucket_iam_member" "backup_member_on_storage" {
  count      = var.provision_backup_bucket ? 1 : 0
  bucket     = google_storage_bucket.clickhouse_data.name
  role       = "roles/storage.objectUser"
  member     = google_service_account.backup_service_account[0].member
  depends_on = [google_storage_bucket.clickhouse_data, google_service_account.backup_service_account]
}
