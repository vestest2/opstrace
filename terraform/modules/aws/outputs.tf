output "clickhouse_s3_user_secret" {
  value     = aws_iam_access_key.clickhouse-s3-user-key.secret
  sensitive = true
}

output "clickhouse_s3_user_id" {
  value     = aws_iam_access_key.clickhouse-s3-user-key.id
  sensitive = true
}

output "clickhouse_s3_secret" {
  value = kubernetes_secret.clickhouse-s3-user-secret.id
}

output "clickhouse_s3_bucket_endpoint_url" {
  value = "https://${aws_s3_bucket.clickhouse-s3-bucket.id}.s3.${aws_s3_bucket.clickhouse-s3-bucket.region}.amazonaws.com/${aws_s3_object.root-bucket.id}"
}
