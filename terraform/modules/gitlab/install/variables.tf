variable "public_ip" {
  description = "Public IP of the VM on which Gitlab is to be installed."
  type        = string
}

variable "ssh_username" {
  description = "Username that will be used to SSH onto the VM."
  type        = string
}

variable "ssh_private_key" {
  description = "Private Key used to SSH onto the VM."
  type        = string
  sensitive   = true
}

variable "instance_name" {
  description = "Name of the VM instance on which Gitlab is to be installed."
  type        = string
}

variable "registry_username" {
  description = "Username to be used for docker image registry."
  type        = string
  sensitive   = true
}

variable "registry_auth_token" {
  description = "Auth token to be used to pull image from registry."
  type        = string
  sensitive   = true
}

variable "domain" {
  description = "Domain name to be used as hostname for Gitlab."
  type        = string
}

variable "gitlab_image" {
  description = "Specific Gitlab image that will be installed."
  type        = string
}

variable "gob_domain" {
  description = "Domain name for the Observability Cluster"
  type        = string
}
