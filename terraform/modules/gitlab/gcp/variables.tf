variable "instance_name" {
  description = "A unique name that will be appended to all managed resources."
  type        = string
}

variable "project_id" {
  description = "The ID of the project in which to provision resources."
  type        = string
}

# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "domain" {
  description = "Domain for hosting gitlab"
  type        = string
}

variable "cloud_dns_zone" {
  description = "The name of the Cloud DNS managed zone to add the NS A record pointing at the instance"
  type        = string
}
