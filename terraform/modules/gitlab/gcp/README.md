# Standalone VM instance on GCP

This terraform module creates an instance in GCP, adds a DNS A record to a GCP Cloud DNS to point at the newly created instance, and creates firewall rules to send SSH, HTTP and HTTPS traffic to the instance.
## Outputs

The following terraform outputs are available through `terraform output`:
- `public_ip`
- `ssh_private_key`