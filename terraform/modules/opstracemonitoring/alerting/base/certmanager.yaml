# Based on https://github.com/canada-ca-terraform-modules/terraform-kubernetes-cert-manager/
---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-certmanager-rules
spec:
  groups:
  - name: certmanager
    rules:
    - alert: CertManagerCertFailingToRenew
      annotations:
        title: Cert-manager failed to renew a certificate
        message: 'The cert-manager certificate {{ $labels.name }} is failing to renew'
      expr: |
        (sum by (name, cluster) (time() > certmanager_certificate_renewal_timestamp_seconds) ) AND (sum by (name, cluster) (time() < certmanager_certificate_expiration_timestamp_seconds))
      for: 10m
      labels:
        alertname: CertManagerCertFailingToRenew
        type: cert-manager
        severity: s2
        alert_type: cause

    - alert: CertManagerCertFailure
      annotations:
        title: Certificate is not Ready
        message: 'The cert-manager certificate named {{ $labels.name }} has a ready status that is not true and the current date has not passed the renewal date of the certificate'
      expr: sum by (name, cluster) (certmanager_certificate_ready_status{condition="True"}==0) AND (sum by (name, cluster) (time() < certmanager_certificate_renewal_timestamp_seconds))
      for: 10m
      labels:
        alertname: CertManagerCertFailure
        type: cert-manager
        severity: s2
        alert_type: cause

    - alert: CertManagerCertExpired
      annotations:
        title: Certificate has expired
        message: 'The cert-manager certificate named {{ $labels.name }} has expired.'
      expr: sum by (name, cluster) (time() > certmanager_certificate_expiration_timestamp_seconds)
      for: 2m
      labels:
        alertname: CertManagerCertExpired
        type: cert-manager
        severity: s2
        alert_type: cause

    # Found this online https://gitlab.com/uneeq-oss/cert-manager-mixin
    - alert: CertManagerAbsent
      annotations:
        title: Cert-manager is not running
        message: 'cert-manager has disappeared from Prometheus service discovery.'
      expr: sum by (cluster) (prometheus_heartbeat) unless (sum by (cluster) (up{job="cert-manager"} == 1))
      for: 2m
      labels:
        alertname: CertManagerAbsent
        type: cert-manager
        severity: s2
        alert_type: cause

    - alert: CertManagerHittingRateLimits
      annotations:
        title: Cert-manager is hitting letsencrypt rate limits.
        message: 'Cert manager hitting LetsEncrypt rate limits. Depending on the rate limit, cert-manager may be unable to generate certificates for up to a week. The rate is: {{ $value }}'
      expr: sum by (host, cluster) (rate(certmanager_http_acme_client_request_count{status="429"}[5m])) > 0
      for: 5m
      labels:
        alertname: CertManagerHittingRateLimits
        type: cert-manager
        severity: s2
        alert_type: cause
